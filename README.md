# NodeKEY - Node.js Key Encryption and Yank

[![License](https://img.shields.io/npm/l/nodekey.svg)](LICENSE)
[![Version](https://img.shields.io/npm/v/nodekey.svg)](https://www.npmjs.com/package/nodekey)
[![Downloads](https://img.shields.io/npm/dt/nodekey.svg)](https://www.npmjs.com/package/nodekey)

NodeKEY targets power users that don't like password managers with long startup times, bloated GUIs, lack of keyboard
control or metadata information leaks. It focuses on generated keys instead of stored ones and utilizes a file-system
alike hierarchy with unix alike commands. KISS.

## Installation

```
npm install -g nodekey
```

Run `nodekey --help` to get usage instructions regarding program arguments.

Run `nodekey` without arguments to define a master key and enter interactive mode. Use the interactive command `help`
to show the available commands.

## Features

Main Features:

- Keep your **keys** (passwords) organized with a file-system-alike hierarchy
- Each **application** (directory-alike) may contain a schema with details on key (file-alike) generation
- Configure an application once, generate any amount of keys for it
- Keys can also be stored encrypted if you don't want to generate a pseudo-random one
- Easy to use interactive mode with clipboard utilization
- TXT (no structural information loss) and CSV (passwords only, for compatibility with other managers) exports

Anti-Features:

- Just a desktop application (written for node.js), no smartphone app, no web client
  - A simple webserver for read-only access is planned
- No built-in synchronization
- No CSV imports yet (coming soon)

As of now it's best suited if you avoid synchronization problems by other means.
You may for example use some tool like [syncthing](https://syncthing.net/), especially if you have some always-online
device.
Otherwise it's recommended to use all devices but one read-only -- for each profile. If for example you have one PC for
work and one PC for everything else, create a dedicated profile for work applications.

## Usage

### Initialization

On first startup you are asked for an encryption password for the profile data (e.g. application schemas, meta data,
...). This password will also be used as default master key for key generation and encryption. You will be asked for
this password on every startup with the same profile path (`$HOME/.config/NodeKEY/` by default).

If needed, the `su` command can be used for a different master key for key generation and encryption.

### Settings

You may adjust any settings within the newly created settings file (`<profile-path>/settings.json` by default). However
the default settings should suffice in general.

### Applications and Keys

Use the `mkapp` command (see `help mkapp` for details) to create your applications (use `-S` option for directories that
won't directly contain generated keys). Now you can create keys for those applications with the `key` command (see
`help key` for details).

With the `cat` and `fetch` commands the key contents can be displayed or put into the system clipboard respectively.

## Security

### Generated keys

NodeKEY uses `scrypt` for primary hashing. This provides high computational and memory difficulties for possible
attacks. In addition -- if supported by the system -- `SHA512` or `SHA256` (fallback) is used, just in case `scrypt` has
any design flaw.

### Stored keys

Stored keys use the same hashing methods as generated keys, to generate a passphrase that in turn is used for the
encryption algorithm. NodeKEY already considers it impossible for an attacker to reconstruct the master key based on a
hash.
In conclusion it ought to be impossible to reconstruct the master key from a stored key as well.

For encryption, NodeKEY uses `aes-256-cbc` and `bf-cbc` as available on the system.

As the entropy of a generated key is as high as it gets (NodeKEY might have slight entropy losses, but nothing critical)
with the defined schema, generated keys are still preferred over custom stored ones. Keep in mind that you probably
never need to type in the password by hand.

### Profile

The whole user profile is encrypted as well. This ensures that without master key you cannot see any meta information
such as application names, etc.. This encryption uses the same process described for stored keys above; Algorithm
details and initial vector are stored in a separate file.

### Clipboard

Using the clipboard is one weakness of password managers. An infected system may spy the clipboard and thus get access
to the application keys. However since such malware probably would also spy on typed keys, this weakness is no more
critical than typing passwords by hand.

### Session

Make sure to log out (`Ctrl+D`/`exit`) whenever you leave your computer unwatched. Nobody would be able to get access to
the master key, but the application keys are vulnerable to simple computer access while logged in.
