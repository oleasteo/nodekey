# Change Log

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/) and this project adheres to
[Semantic Versioning](http://semver.org/). Only changes that affect the documentation may be considered to be breaking.

## Unreleased

### Added

- Export command (TXT and CSV)
- Use more secure scrypt options for new stored keys (doubled CPU power, block size and parallelization; quadrupled
  memory upper bound)

### Fixed

- CipherMethod serialization
- Key serialization

### Internal

- Enforce strict typescript mode
- Added prettier & eslint with pre-commit hook

## [1.1.1] - 2019-02-22

### Added

- Hash command for simple integration checks (e.g. compare with forked profile)

### Fixed

- Path arguments did not work properly
- Move command on applications did fail for rename
- `ls` command for keys

## [1.1.0] - 2018-12-03

### Added

- Auto-completion menu for multiple matching suffices

## [1.0.2] - 2018-11-28

### Fixed

- Persist after schema edit
- Do not shut down on key-content fetch when schema is missing

## [1.0.1] - 2018-11-04

### Fixed

- Add `#!` prefix to bin file for proper execution

## [1.0.0] - 2018-11-04

### Added

- Typescript support

### Changed

- Reworked whole application
