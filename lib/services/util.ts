interface FlagsMap {
  [flag: string]: { idx: number; argv: string[] };
}

export interface Flags {
  map: FlagsMap;
  remainder: string[];
  dash: number;
}

export class Deferred<T> {
  promise: Promise<T>;
  resolve!: (value: T) => void;
  reject!: (reason?: unknown) => void;

  constructor() {
    this.promise = new Promise<T>((resolve, reject) => {
      this.resolve = resolve;
      this.reject = reject;
    });
  }
}

export function parseFlags(
  parameters: string[],
  options?: { [char: string]: number }
): Flags {
  const map: FlagsMap = {};
  const remainder: string[] = [];
  let order = 0;
  let dash = 0;
  for (let i = 0; i < parameters.length; i++) {
    const current = parameters[i];
    if (current.length > 0 && current[0] === "-") {
      if (current.length === 1) {
        dash++;
      } else {
        for (let j = 1; j < current.length; j++) {
          const char = current[j];
          if (options != null && Reflect.has(options, char)) {
            map[char] = {
              idx: ++order,
              argv: parameters.slice(i + 1, i + 1 + options[char]),
            };
            i += options[char];
          } else {
            map[current[j]] = { idx: ++order, argv: [] };
          }
        }
      }
    } else {
      remainder.push(current);
    }
  }
  return {
    map,
    remainder,
    dash,
  };
}
