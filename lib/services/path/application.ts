import { getApplications, getRootApplications } from "../profile";
import Path, { ROOT } from "./Path";
import { Application } from "../../types/Application";
import { Key } from "../../types/Key";
import { getState } from "../state";
import CommandError from "../../types/CommandError";

export interface PathStats {
  isRoot: boolean;
  isApp: boolean;
  isKey: boolean;
  isResolved: boolean;
  app: Application | null;
  key: Key | null;
  relPath: Path | null;
}

export function appFromPath(path: Path): Application | null {
  if (!path.absolute) {
    throw new Error("Absolute path expected.");
  }
  let app: Application | null = null;
  let apps = getRootApplications();
  for (let i = path.reverseParts.length - 1; i >= 0; i--) {
    const name = path.reverseParts[i];
    if (!Reflect.has(apps, name)) {
      throw new CommandError("No such application: " + path.toString());
    }
    app = apps[name];
    apps = app.children;
  }
  return app;
}

export function pathFromAppId(appId: string | null): Path {
  if (appId === null) {
    return ROOT;
  }
  const applications = getApplications();
  return pathFromApp(applications[appId]);
}

export function pathFromApp(
  application: Application | null,
  apps = getApplications()
): Path {
  if (application === null) {
    return ROOT;
  }
  const path = new Path();
  const reverseParts = path.reverseParts;
  path.absolute = true;
  // eslint-disable-next-line no-constant-condition
  while (true) {
    reverseParts.push(application.name);
    if (application.parent === null) {
      return path;
    }
    application = apps[application.parent];
  }
}

export function readStats(path: Path, cwd?: Application | null) {
  const result = {
    isRoot: false,
    isApp: false,
    isKey: false,
    isResolved: true,
    app: null,
    key: null,
    relPath: null,
  } as PathStats;
  const relPath = path.clone();
  const app = (result.app = rebase(relPath, cwd));
  if (app === null) {
    result.isRoot = result.isApp = true;
    if (relPath.reverseParts.length !== 0) {
      result.isResolved = false;
      result.relPath = relPath;
    }
    return result;
  }
  if (relPath.reverseParts.length === 0) {
    result.isApp = true;
    if (app.defaultKeyId !== null) {
      result.key = app.keysList.find(
        (key) => key._id === app.defaultKeyId
      ) as Key;
    }
    return result;
  }
  if (
    relPath.reverseParts.length === 1 &&
    Reflect.has(app.keys, relPath.reverseParts[0])
  ) {
    result.isKey = true;
    result.key = app.keys[relPath.reverseParts[0]];
    return result;
  }
  result.isResolved = false;
  result.relPath = relPath;
  return result;
}

/**
 * Resolves the deepest existing application within the provided path. The path is modified in-place to describe the
 * same destination path but relative to the returned application.
 *
 * @param path The path to re-base.
 * @param cwd The CWD to use if path is relative.
 */
export function rebase(
  path: Path,
  cwd: Application | null = appFromPath(getState().cwd)
): Application | null {
  const applications = getApplications();
  const rootApplications = getRootApplications();
  let app = path.absolute ? null : cwd;
  while (path.up > 0) {
    if (app === null) {
      throw new CommandError("Path cannot resolve higher than root.");
    }
    app = app.parent === null ? null : applications[app.parent];
    path.up--;
  }
  let children = app === null ? rootApplications : app.children;
  const parts = path.reverseParts;
  for (let i = parts.length - 1; i >= 0; i--) {
    const name = parts[i];
    if (!Reflect.has(children, name)) {
      break;
    }
    parts.pop();
    app = children[name];
    children = app.children;
  }
  return app;
}
