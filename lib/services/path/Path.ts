import { getData } from "../profile";
import { pathFromAppId } from "./application";
import CommandError from "../../types/CommandError";

export default class Path {
  absolute = false;
  reverseParts: string[];
  up = 0; // number of '../' that prefix this Path

  constructor() {
    this.reverseParts = [];
  }

  toString(trailingSlash = false): string {
    const relativeString = buildPathStringReverse(this.reverseParts);
    if (this.absolute) {
      return (
        "/" +
        relativeString +
        (trailingSlash && relativeString.length > 0 ? "/" : "")
      );
    }
    let prefix = this.up === 0 ? "." : "..";
    for (let i = 1; i < this.up; i++) {
      prefix += "/..";
    }
    if (this.reverseParts.length === 0) {
      return prefix + (trailingSlash ? "/" : "");
    }
    return (
      prefix +
      "/" +
      relativeString +
      (trailingSlash && relativeString.length > 0 ? "/" : "")
    );
  }

  clone(): Path {
    const clone = new Path();
    clone.reverseParts = this.reverseParts.slice();
    clone.absolute = this.absolute;
    clone.up = this.up;
    return clone;
  }

  equals(other: Path): boolean {
    if (
      this.up !== other.up ||
      this.absolute !== other.absolute ||
      this.reverseParts.length !== other.reverseParts.length
    ) {
      return false;
    }
    for (const i in this.reverseParts) {
      if (this.reverseParts[i] !== other.reverseParts[i]) {
        return false;
      }
    }
    return true;
  }

  parent(): Path {
    const parent = this.clone();
    if (parent.reverseParts.length > 0) {
      parent.reverseParts.shift();
    } else {
      parent.up++;
      if (parent.absolute) {
        throw new CommandError("Path cannot resolve higher than root.");
      }
    }
    return parent;
  }

  append(path: Path): this {
    if (path.absolute) {
      this.absolute = true;
      this.reverseParts = path.reverseParts.slice();
      this.up = 0;
    } else if (path.up === 0) {
      if (path.reverseParts.length > 0) {
        this.reverseParts.unshift(...path.reverseParts);
      }
    } else {
      if (path.up >= this.reverseParts.length) {
        this.up += path.up - this.reverseParts.length;
        if (this.up > 0 && this.absolute) {
          throw new CommandError("Path cannot resolve higher than root.");
        }
        this.reverseParts = path.reverseParts.slice();
      } else {
        if (path.reverseParts.length > 0) {
          this.reverseParts.splice(0, path.up, ...path.reverseParts);
        } else {
          this.reverseParts.splice(0, path.up);
        }
      }
    }
    return this;
  }

  prepend(path: Path): this {
    if (this.absolute) {
      return this;
    }
    this.absolute = path.absolute;
    if (this.up === 0) {
      if (path.reverseParts.length > 0) {
        this.reverseParts.push(...path.reverseParts);
      }
    } else {
      if (this.up >= path.reverseParts.length) {
        this.up = this.up - path.reverseParts.length + path.up;
      } else {
        if (path.reverseParts.length > 0) {
          this.reverseParts.push(...path.reverseParts.slice(this.up));
        }
        this.up = 0;
      }
    }
    return this;
  }

  static fromString(path: string, considerBookmark = true): Path {
    const target = new Path();
    if (path.length === 0) {
      return target;
    }
    normalizeParts(target, path, considerBookmark);
    return target;
  }

  static join(from: Path, ...to: Path[]): Path {
    const combined = from.clone();
    for (let i = 0; i < to.length; i++) {
      combined.append(to[i]);
    }
    return combined;
  }
}

export const ROOT = new Path();
ROOT.absolute = true;

function buildPathStringReverse(reverseParts: string[]): string {
  let path = "";
  for (let i = reverseParts.length - 1; i >= 0; i--) {
    if (path.length > 0) {
      path += "/";
    }
    path += reverseParts[i];
  }
  return path;
}

function normalizeParts(target: Path, path: string, considerBookmark: boolean) {
  const parts = path.split("/");
  for (let i = parts.length - 1; i >= 0; i--) {
    normalizePart(target, parts[i], i === 0, considerBookmark);
  }
}

function normalizePart(
  target: Path,
  part: string,
  isFirstPart: boolean,
  considerBookmark: boolean
): void {
  if (part === ".") {
    return;
  }
  if (part === "..") {
    target.up++;
    return;
  }
  if (part[0] === "-") {
    throw new CommandError("Path name cannot begin with '-'.");
  }
  if (isFirstPart) {
    if (part.length === 0) {
      // leading '/'
      target.absolute = true;
      if (target.up > 0) {
        throw new CommandError("Path cannot resolve higher than root.");
      }
      return;
    } else if (considerBookmark) {
      // check for leading bookmark
      const bookmarks = getData().bookmarks;
      if (Reflect.has(bookmarks, part)) {
        target.prepend(pathFromAppId(bookmarks[part]));
        return;
      } else if (part === "~") {
        target.prepend(ROOT); // fallback to ROOT for '~' if no bookmark
        return;
      }
    }
  } else {
    if (part.length === 0) {
      return;
    } // skip multiple slashes
    // skip part if '..' demands this
    if (target.up > 0) {
      target.up--;
      return;
    }
  }
  // normal part => prepend
  target.reverseParts.push(part);
  return;
}
