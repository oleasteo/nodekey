import Path from "./path/Path";

let current: HierarchyState | null = null;

interface HierarchyState extends State {
  parent: HierarchyState | null;
  depth: number;
}

export interface State extends PushState {
  history: string[];
}

export interface PushState {
  masterKey: Buffer;
  cwd: Path;
}

export function pushState(state: PushState): void {
  const parent = current;
  current = state as HierarchyState;
  current.history = [];
  current.parent = parent;
  current.depth = parent === null ? 1 : parent.depth + 1;
}

export function putState(state: Partial<State>): void {
  if (current === null) {
    throw new Error("Cannot merge state without initial state.");
  }
  Object.assign(current, state);
}

export function dropState(): State | null {
  if (current === null) {
    return null;
  }
  current = current.parent;
  return current;
}

export function getState(): HierarchyState {
  if (current === null) {
    throw new Error("State must exist for request.");
  }
  return current;
}
