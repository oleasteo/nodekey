import { getState } from "./state";
import Path from "./path/Path";
import { appFromPath, rebase } from "./path/application";
import { Application } from "../types/Application";
import { getApplications, getData, getRootApplicationsList } from "./profile";
import { Key } from "../types/Key";
import { CompletionArray, CompletionResult } from "terminal-kit";

export function completeList(input: string, list: string[]): CompletionResult {
  const matchingList =
    input === null
      ? list
      : (list.filter((entry) => entry.startsWith(input)) as CompletionArray);
  return _completeList(matchingList, input.length);
}

export function completeKey(
  input: string | null,
  cwd?: Application | null
): CompletionResult {
  if (input === null) {
    return "";
  }
  if (input.endsWith(".")) {
    return "";
  }
  const { prefixLength, appChildren, keyChildren, bookmarks } = getChildren(
    input,
    cwd
  );
  const matchingNames = bookmarks
    .map((bookmark) => bookmark + "/")
    .concat(appChildren.map((child) => child.name + "/"))
    .concat(keyChildren.map((child) => child.name));
  return _completeList(matchingNames, prefixLength);
}

export const completePath = completeKey;

export function completeApp(
  input: string | null,
  cwd?: Application | null
): CompletionResult {
  if (input === null) {
    return "";
  }
  if (input.endsWith(".")) {
    return "";
  }
  const { prefixLength, appChildren, bookmarks } = getChildren(input, cwd);
  const matchingNames = bookmarks
    .map((bookmark) => bookmark + "/")
    .concat(appChildren.map((child) => child.name + "/"));
  return _completeList(matchingNames, prefixLength);
}

function getChildren(input: string, cwd?: Application | null) {
  const applications = getApplications();
  const considerBookmark = input.includes("/");
  const path = Path.fromString(input, considerBookmark);
  const useBookmarks = !considerBookmark && path.reverseParts.length <= 1;
  if (typeof cwd === "undefined") {
    cwd = appFromPath(getState().cwd);
  }
  // resolve application
  let app: Application | null;
  try {
    app = rebase(path, cwd);
  } catch (ignored) {
    // no result for failed rebase (higher than root error)
    return {
      prefixLength: input.length,
      appChildren: [] as Application[],
      keyChildren: [] as Key[],
      bookmarks: [],
    };
  }
  // if an app was resolved without trailing '/', use its parent instead
  if (
    path.reverseParts.length === 0 &&
    app !== null &&
    input.endsWith(app.name)
  ) {
    path.reverseParts.push((app as Application).name);
    const parentId = (app as Application).parent;
    app = parentId === null ? null : applications[parentId];
  }
  // collect children
  let appChildren = app === null ? getRootApplicationsList() : app.childrenList;
  let keyChildren = app === null ? [] : app.keysList;
  let bookmarks = useBookmarks ? Object.keys(getData().bookmarks) : [];
  // filter for matching prefix
  const pathSuffix = path.reverseParts[0];
  if (path.reverseParts.length > 0) {
    appChildren = appChildren.filter((child) =>
      child.name.startsWith(pathSuffix)
    );
    keyChildren = keyChildren.filter((child) =>
      child.name.startsWith(pathSuffix)
    );
    bookmarks = bookmarks.filter((bookmark) => bookmark.startsWith(pathSuffix));
  }
  return {
    prefixLength: path.reverseParts.length === 0 ? 0 : pathSuffix.length,
    appChildren,
    keyChildren,
    bookmarks,
  };
}

function _completeList(
  matchingList: string[],
  prefixLength: number
): CompletionResult {
  if (matchingList.length === 0) {
    return "";
  }
  if (matchingList.length === 1) {
    return matchingList[0].substring(prefixLength);
  }
  const commonLength = commonPrefixLength(matchingList, prefixLength);
  if (commonLength > prefixLength) {
    // apply (partial) completion
    return matchingList[0].substring(prefixLength, commonLength);
  }
  // suggest possible completions
  const result = matchingList as CompletionArray;
  result.sort();
  result._truncate = prefixLength;
  return result;
}

function commonPrefixLength(list: string[], offset = 0): number {
  const ref = list[0];
  for (; offset < ref.length; offset++) {
    const refChar = ref[offset];
    for (let i = 1; i < list.length; i++) {
      if (list[i][offset] !== refChar) {
        return offset;
      }
    }
  }
  return offset;
}
