import { readSync, write, writeSync } from "clipboardy";

export async function writeClipboard(input: Buffer) {
  return await write(input.toString());
}

/**
 * Synchronously clears the clipboard if the content matches the passed input.
 *
 * @param input The input to clear.
 */
export function clearClipboard(input: Buffer | null) {
  if (input === null) {
    return writeSync("");
  }
  const value = readSync();
  if (value === input.toString()) {
    writeSync("");
  }
}
