import { defaultsDeep } from "lodash";
import { promises as fs } from "fs";
import { BaseSettings, Settings } from "../types/Settings";
import defaultSettings from "../constants/defaultSettings";

let settings: BaseSettings;

export async function init(file: string, interactive: boolean) {
  let obj = await readSettings(file, interactive);
  if (interactive && Reflect.has(obj, "interactive")) {
    obj = defaultsDeep(obj.interactive, obj);
  }
  settings = obj as BaseSettings;
}

async function readSettings(
  file: string,
  writeDefaults: boolean
): Promise<Settings> {
  const defSettings = defaultSettings();
  try {
    await fs.access(file);
  } catch (ignored) {
    if (writeDefaults) {
      await fs.writeFile(file, JSON.stringify(defSettings, null, 2));
    }
    return defSettings;
  }
  return defaultsDeep(
    JSON.parse((await fs.readFile(file)).toString()),
    defSettings
  );
}

export function getSettings() {
  return settings;
}
