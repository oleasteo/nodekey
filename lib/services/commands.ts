import CMD_ALIAS from "../commands/alias";
import CMD_APPLICATION from "../commands/application/index";
import CMD_PATH from "../commands/path/index";
import CMD_BOOKMARK from "../commands/bookmark";
import CMD_HELP from "../commands/help";
import CMD_INTEGRITY from "../commands/integrity";
import CMD_KEY from "../commands/key/index";
import CMD_NAVIGATION from "../commands/navigation";
import CMD_SHELL from "../commands/shell";
import CMD_EXPORT from "../commands/export/export";

import { CommandCallback, CommandExecutor } from "../types/CommandExecutor";
import { terminal } from "terminal-kit";
import { EOL } from "os";
import { flatten } from "lodash";
import { getData } from "./profile";
import CommandError from "../types/CommandError";

const EXECUTORS = ([] as CommandExecutor[]).concat(
  CMD_HELP,
  CMD_SHELL,
  CMD_INTEGRITY,
  CMD_NAVIGATION,
  CMD_BOOKMARK,
  CMD_ALIAS,

  CMD_APPLICATION,
  CMD_KEY,
  CMD_PATH,

  CMD_EXPORT
);
const BASE_COMMANDS = flatten(EXECUTORS.map((ex) => ex.cmd));
const ALL_CONTINUE: ExecuteResult = { success: true, cont: true };
const SEMICOLON_SPLIT_REGEX = /(?:\\.|"(?:\\.|[^"])*(?:"|$)|'(?:\\.|[^'])*(?:'|$)|[^;])+/g;
const SPLIT_QUOTES_REGEX = /(?:\\.|"(?:\\.|[^"])*(?:"|$)|'(?:\\.|[^'])*(?:'|$)|\S)+/g;
const UN_ESCAPE_REGEX = /(?:\\(.)|"((?:\\.|[^"])*)(?:"|$)|'((?:\\.|[^'])*)(?:'|$))/g;

interface ExecuteResult {
  success: boolean;
  cont: boolean;
}

interface Command extends Array<string> {
  trailingSpace: boolean;
}

export function collectCommands() {
  return BASE_COMMANDS.concat(Object.keys(getData().aliases));
}

export async function execFn(
  fn: CommandCallback,
  argv: string[],
  parameters: string[] = []
): Promise<ExecuteResult> {
  try {
    let result = fn(parameters, argv, EXECUTORS);
    if (result != null && typeof result !== "boolean") {
      result = await result;
    }
    return { success: true, cont: result !== false };
  } catch (error) {
    if (!(error instanceof CommandError)) {
      throw error;
    }
    terminal.error.red(error.message)(EOL);
    return { success: false, cont: error.cont };
  }
}

/**
 * @returns {boolean} Whether all commands have succeeded.
 */
export async function execAll(input: string[]): Promise<boolean> {
  for (const cmd of input) {
    const result = await exec(parseCommand(cmd));
    if (!result.success) {
      return false;
    }
  }
  return true;
}

/**
 * Splits the passed input into sub-commands (separated by `;`) and prepares those sub-commands for {@link exec}.
 *
 * @param input The input string to prepare.
 */
export function parseCommand(input: string): Command[] {
  const commandStrings = input.match(SEMICOLON_SPLIT_REGEX) || [];
  if (
    commandStrings.length > 0 &&
    !input.endsWith(commandStrings[commandStrings.length - 1])
  ) {
    // input ends with ';' => add empty command string
    commandStrings.push("");
  }
  return commandStrings.map((commandString) => {
    const argv = splitArguments(commandString) as Command;
    argv.trailingSpace =
      commandString.length > 0 &&
      /\s/.test(commandString[commandString.length - 1]);
    return argv;
  });
}

export async function exec(commands: string[][]): Promise<ExecuteResult> {
  for (const cmd of commands) {
    if (cmd.length === 0) {
      continue;
    }
    const result = await execute(cmd);
    if (!result.success || !result.cont) {
      return result;
    }
  }
  return ALL_CONTINUE;
}

async function execute(argv: string[]): Promise<ExecuteResult> {
  applyMatchingAlias(argv);
  const cmdName = argv[0];
  const parameters = argv.slice(1);
  const executor = getExecutor(cmdName);
  if (executor == null) {
    terminal.red.bold("Command not found: " + cmdName)(EOL);
    return { success: false, cont: false };
  }
  const result = await execFn(executor.callback, argv, parameters);
  if (!result.success || !result.cont) {
    return result;
  }
  return ALL_CONTINUE;
}

export function getExecutor(cmdName: string): CommandExecutor | undefined {
  return EXECUTORS.find((ex) => ex.cmd === cmdName);
}

export function applyMatchingAlias(argv: string[]) {
  const aliases = getData().aliases;
  const cmdName = argv[0];
  if (Reflect.has(aliases, cmdName)) {
    const alias = aliases[cmdName];
    const aliasv = splitArguments(alias);
    argv.splice(0, 1, ...aliasv);
  }
}

function splitArguments(input: string): string[] {
  const dirtyArguments = input.match(SPLIT_QUOTES_REGEX);
  if (dirtyArguments === null) {
    return [];
  }
  return dirtyArguments.map((arg) => arg.replace(UN_ESCAPE_REGEX, "$1$2$3"));
}
