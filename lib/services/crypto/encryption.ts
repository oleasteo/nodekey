import { getCiphers, randomBytes } from "crypto";
import {
  CipherMethod,
  CipherMethodSchema,
  SCryptOptions,
} from "../../types/Method";

const MODERATE_SCRYPT_OPTIONS: SCryptOptions = {
  cost: 32768,
  blockSize: 16,
  parallelization: 2,
  maxmem: 128 * 1024 * 1024,
};

export function methods() {
  const availableCiphers = getCiphers();
  const methods: CipherMethodSchema[] = [];
  if (availableCiphers.includes("bf-cbc")) {
    methods.push({
      algorithm: { name: "bf-cbc", keyBytes: 16 },
      saltBytes: 24,
      ivBytes: 8,
      keyOptions: MODERATE_SCRYPT_OPTIONS,
    });
  }
  if (availableCiphers.includes("aes-256-cbc")) {
    methods.push({
      algorithm: { name: "aes-256-cbc", keyBytes: 32 },
      saltBytes: 24,
      ivBytes: 16,
      keyOptions: MODERATE_SCRYPT_OPTIONS,
    });
  }
  return methods;
}

export function fromSchema(
  schema: CipherMethodSchema,
  salt: Buffer = randomBytes(schema.saltBytes),
  iv: Buffer = randomBytes(schema.ivBytes)
): CipherMethod {
  return {
    algorithm: schema.algorithm,
    salt,
    iv,
    options: schema.options,
    keyOptions: schema.keyOptions,
  };
}
