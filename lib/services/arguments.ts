import * as yargs from "yargs";

export default function parse() {
  return yargs
    .usage("Usage: $0 [options]")
    .alias("e", "execute")
    .array("e")
    .describe("e", "Execute the given command(s) on startup.")
    .alias("p", "profile-path")
    .alias("p", "path")
    .describe("p", "Base path to contain profile files.")
    .string("s")
    .alias("s", "settings")
    .default("s", null, "<profile-path>/settings.json")
    .describe("s", "settings file.")
    .string("m")
    .alias("m", "master")
    .describe("m", "Specify master key.")
    .boolean("i")
    .alias("i", "interactive")
    .default("i", void 0, "true iff no -e argument is passed")
    .describe("i", "Enforce interactive mode")
    .alias("v", "version")
    .boolean("v")
    .describe("v", "Show version and terminate.")
    .epilogue("For help with interactive commands, type `help` interactively.")
    .help()
    .alias("h", "help")
    .describe("h", "Show this help message.")
    .env("NODEKEY")
    .version(false).argv;
}
