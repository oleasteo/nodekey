import {
  InputFieldOptions,
  InputReturn,
  MenuResponse,
  MenuReturn,
  SingleColumnMenuOptions,
  terminal,
} from "terminal-kit";
import { EOL } from "os";
import { Deferred } from "../services/util";

let currentInput: Input | null = null;

interface Input {
  instance: InputReturn | MenuReturn;
  cancel: () => void;
  cancelable: boolean;
}

export async function cleanUp(exitCode?: number): Promise<never> {
  await terminal.asyncCleanup();
  process.exit(exitCode || 0);
}

export async function textInput(options?: InputFieldOptions): Promise<string> {
  const input = terminal.inputField(options);
  const deferred = new Deferred() as Deferred<string>;
  currentInput = {
    instance: input,
    cancel,
    cancelable: !!(options && options.cancelable),
  };
  const result = await Promise.race([input.promise, deferred.promise]).finally(
    () => {
      if (currentInput && currentInput.instance === input) {
        currentInput = null;
      }
    }
  );
  terminal(EOL);
  return result;

  function cancel() {
    input.abort();
    deferred.reject(new Error("cancelled"));
  }
}

export async function singleColumnMenu(
  menuItems: string[],
  options?: SingleColumnMenuOptions
): Promise<MenuResponse> {
  const input = terminal.singleColumnMenu(menuItems, options);
  const deferred = new Deferred() as Deferred<MenuResponse>;
  currentInput = {
    instance: input,
    cancel,
    cancelable: !!(options && options.cancelable),
  };
  const result = await Promise.race([input.promise, deferred.promise]).finally(
    () => {
      if (currentInput && currentInput.instance === input) {
        currentInput = null;
      }
    }
  );
  terminal(EOL);
  return result;

  function cancel() {
    input.abort();
    deferred.reject(new Error("cancelled"));
  }
}

export function getCurrentInput() {
  if (currentInput !== null) {
    if (Reflect.has(currentInput.instance, "getInput")) {
      return (currentInput.instance as InputReturn).getInput();
    } else {
      return -1;
    }
  }
  return null;
}

export function isCancelable() {
  return currentInput === null || currentInput.cancelable;
}

export function cancelInput() {
  if (currentInput === null) {
    return;
  }
  currentInput.cancel();
}
