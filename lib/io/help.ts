import { CommandExecutor } from "../types/CommandExecutor";
import {
  STY_HELP_DESCRIPTION,
  STY_HELP_KEYWORD,
  STY_HELP_SEE,
  STY_DEFAULT,
  STY_HELP_HEAD,
  STY_HELP_NAME,
} from "../constants/styles";
import { EOL } from "os";

const HELP_DESCRIPTION_CHARS_PER_LINE = 78; // 80 minus '| '

export function printHelp(executor: CommandExecutor) {
  STY_HELP_NAME(executor.cmd);
  if (Reflect.has(executor.help, "head")) {
    STY_HELP_HEAD.noFormat(" " + executor.help.head);
  }
  const description = executor.help.description;
  for (
    let i = 0;
    i < description.length;
    i += HELP_DESCRIPTION_CHARS_PER_LINE
  ) {
    STY_DEFAULT(EOL);
    STY_DEFAULT("| ");
    STY_HELP_DESCRIPTION.noFormat(
      description.substr(i, HELP_DESCRIPTION_CHARS_PER_LINE)
    );
  }
  STY_DEFAULT(EOL);
  const seeList = executor.help.see;
  if (seeList && seeList.length > 0) {
    STY_DEFAULT("| ");
    STY_HELP_KEYWORD("See also: ");
    for (let i = 0; i < seeList.length; i++) {
      if (i !== 0) {
        STY_DEFAULT(", ");
      }
      STY_HELP_SEE(seeList[i]);
    }
    STY_DEFAULT(EOL);
  }
}
