import { EOL } from "os";
import {
  STY_ALIAS_CMD,
  STY_ALIAS_NAME,
  STY_APP,
  STY_BOOKMARK_NAME,
  STY_BOOKMARK_PATH,
  STY_DEFAULT,
  STY_HELP_HEAD,
  STY_HELP_NAME,
  STY_INFO_CONTENT,
  STY_INFO_CONTENT_HIDDEN,
  STY_INFO_SALT,
  STY_INTEGRITY_DEFAULT,
  STY_INTEGRITY_NAME,
  STY_KEY_HASHED,
  STY_KEY_STORED,
  STY_PROMPT_META_DEFAULT,
  STY_PROMPT_META_KEY,
  STY_PROMPT_META_VALUE,
} from "../constants/styles";
import { CommandExecutor } from "../types/CommandExecutor";
import { Integrity } from "../types/Profile";
import Path from "../services/path/Path";
import { readStats } from "../services/path/application";
import { getRootApplicationsList } from "../services/profile";
import { Key } from "../types/Key";
import { Meta } from "../types/Meta";
import { Application } from "../types/Application";
import { errPathNotFound } from "./feedback";
import {
  integrityHash,
  integrityHashApp,
  integrityHashKey,
} from "../services/crypto/hashing";

export function printIntegrityList(integrityMap: { [id: string]: Integrity }) {
  if (Reflect.has(integrityMap, "")) {
    STY_INTEGRITY_DEFAULT("default")(EOL);
  }
  for (const key in integrityMap) {
    if (key === "") {
      continue;
    }
    STY_INTEGRITY_NAME(key)(EOL);
  }
}

export function printNavigationList(
  paths: Path[],
  depth: number,
  details: boolean
) {
  for (const path of paths) {
    const stats = readStats(path);
    if (!stats.isResolved) {
      errPathNotFound(path.toString());
      continue;
    }
    if (stats.isApp) {
      if (details || depth > 0 || paths.length > 1) {
        STY_APP.bold(path.toString(true));
        if (details && stats.app !== null) {
          STY_DEFAULT("\t");
          printMetaLine(stats.app.meta, false);
        }
        STY_DEFAULT(EOL);
        printNavigationPath(stats.app, depth, details, "  ");
      } else {
        printNavigationPath(stats.app, depth, details, "");
      }
    } else if (stats.isKey) {
      printNavigationKey(
        stats.key as Key,
        stats.app as Application,
        details,
        ""
      );
    }
  }
}

export function printHashList(
  paths: Path[],
  depth: number,
  every: boolean,
  prefix = ""
) {
  const hashes: Buffer[] = [];
  const single = paths.length === 1;
  for (const path of paths) {
    const stats = readStats(path);
    if (!stats.isResolved) {
      errPathNotFound(path.toString());
      continue;
    }
    if (stats.isApp) {
      hashes.push(_printHashList(stats.app, true, every, depth, prefix));
    } else if (stats.isKey) {
      hashes.push(_printHashKey(stats.key as Key, every, prefix));
    }
  }
  if (!single) {
    STY_PROMPT_META_VALUE(integrityHash(hashes).toString("hex"));
    STY_DEFAULT("\t");
    STY_DEFAULT.bold("[total]");
    STY_DEFAULT(EOL);
  }
}

function _printHashList(
  app: Application | null,
  self: boolean,
  every: boolean,
  depth: number,
  prefix: string
): Buffer {
  const selfHash = app && integrityHashApp(app);
  const hashes: Buffer[] = selfHash ? [selfHash] : [];
  depth--;
  const showDeep = every && depth >= 0;
  const children = app === null ? getRootApplicationsList() : app.childrenList;
  const keys = app === null ? ([] as Key[]) : app.keysList;
  children.sort(compareByName);
  keys.sort(compareByName);
  for (const child of children) {
    hashes.push(_printHashList(child, showDeep, every, depth, prefix + "  "));
  }
  for (const key of keys) {
    hashes.push(_printHashKey(key, showDeep, prefix + "  "));
  }
  const hash = integrityHash(hashes);
  if (self) {
    STY_PROMPT_META_VALUE(hash.toString("hex"));
    STY_DEFAULT("\t");
    STY_DEFAULT(prefix);
    STY_APP(app == null ? "/" : app.name + "/");
    STY_DEFAULT(EOL);
  }
  return hash;
}

function _printHashKey(key: Key, self: boolean, prefix: string): Buffer {
  const hash = integrityHashKey(key);
  if (self) {
    STY_PROMPT_META_VALUE(hash.toString("hex"));
    STY_DEFAULT("\t");
    STY_DEFAULT(prefix);
    if (key.hashed) {
      STY_KEY_HASHED(key.name);
    } else {
      STY_KEY_STORED(key.name);
    }
    STY_DEFAULT(EOL);
  }
  return hash;
}

function printNavigationPath(
  app: Application | null,
  depth: number,
  details: boolean,
  prefix: string
) {
  const children = app === null ? getRootApplicationsList() : app.childrenList;
  const keys = app === null ? ([] as Key[]) : app.keysList;
  children.sort(compareByName);
  keys.sort(compareByName);
  depth--;
  for (const child of children) {
    printNavigationApp(child, depth, details, prefix);
  }
  for (const key of keys) {
    printNavigationKey(key, app as Application, details, prefix);
  }
}

function printNavigationApp(
  app: Application,
  depth: number,
  details: boolean,
  prefix: string
) {
  STY_DEFAULT(prefix);
  STY_APP(app.name + "/");
  if (details) {
    STY_DEFAULT("\t");
    printMetaLine(app.meta, false);
  }
  STY_DEFAULT(EOL);
  if (depth >= 0) {
    printNavigationPath(app, depth, details, prefix + "  ");
  }
}

function printNavigationKey(
  key: Key,
  app: Application,
  details: boolean,
  prefix: string
) {
  STY_DEFAULT(prefix);
  if (key.hashed) {
    STY_KEY_HASHED(key.name);
  } else {
    STY_KEY_STORED(key.name);
  }
  if ((app as Application).defaultKeyId === key._id) {
    STY_DEFAULT(" [default]");
  }
  if (details) {
    STY_DEFAULT("\t");
    printMetaLine(key.meta, false);
  }
  STY_DEFAULT(EOL);
}

export function printAlias(name: string, cmd: string) {
  STY_ALIAS_NAME.noFormat(name);
  STY_DEFAULT.noFormat("\t");
  STY_ALIAS_CMD.noFormat(cmd);
  STY_DEFAULT(EOL);
}

export function printBookmark(name: string, path: string) {
  STY_BOOKMARK_NAME.noFormat(name);
  STY_DEFAULT.noFormat("\t");
  STY_BOOKMARK_PATH.noFormat(path);
  STY_DEFAULT(EOL);
}

export function printHelpList(executors: CommandExecutor[]) {
  STY_DEFAULT("Available commands:")(EOL);
  for (const executor of executors) {
    STY_DEFAULT(" ");
    STY_HELP_NAME(executor.cmd);
    if (Reflect.has(executor.help, "head")) {
      STY_HELP_HEAD.noFormat(" " + executor.help.head);
    }
    STY_DEFAULT(EOL);
  }
}

export function printApplicationEditSchema(name: string) {
  STY_DEFAULT("Define schema for application: ");
  STY_APP(name)(EOL);
}

export function printOnceSalt(salt: Buffer) {
  STY_DEFAULT("Used salt: ");
  STY_INFO_SALT(salt.toString("hex"))(EOL);
}

export function printKeyContent(content: Buffer, hidden: boolean) {
  if (hidden) {
    STY_INFO_CONTENT_HIDDEN(content.toString())(EOL);
  } else {
    STY_INFO_CONTENT(content.toString())(EOL);
  }
}

export function printDefaultKey(key: Key) {
  STY_DEFAULT("Default key: ");
  printKeyName(key);
  STY_DEFAULT(EOL);
}

export function printMetaLine(meta: Meta[], eol = true) {
  let first = true;
  for (const m of meta) {
    if (!first) {
      STY_PROMPT_META_DEFAULT(", ");
    }
    STY_PROMPT_META_KEY(m.name);
    STY_PROMPT_META_DEFAULT(" ");
    STY_PROMPT_META_VALUE(m.value);
    first = false;
  }
  eol && STY_DEFAULT(EOL);
}

export function printKeyName(key: Key) {
  if (key.hashed) {
    STY_KEY_HASHED(key.name);
  } else {
    STY_KEY_STORED(key.name);
  }
}

function compareByName(a: { name: string }, b: { name: string }) {
  return a.name.localeCompare(b.name);
}
