import { STY_DEFAULT, STY_ERROR } from "../constants/styles";
import { singleColumnMenu, textInput } from "./terminal";
import { KeySchema } from "../types/Application";
import { EOL } from "os";
import { prompt } from "inquirer";
import CommandError from "../types/CommandError";
import { getSettings } from "../services/settings";
import { getState } from "../services/state";
import { encrypt } from "../services/crypto";
import { CipherMethod } from "../types/Method";
import { CharSet, CustomCharSet, isCustomCharSet } from "../types/Settings";
import { terminal } from "terminal-kit";

export async function queryMasterKey() {
  STY_DEFAULT("Insert master key: ");
  return Buffer.from(await textInput({ echo: false }));
}

export async function queryKeySchema(
  overwrite?: KeySchema
): Promise<KeySchema> {
  let length = 0;
  let i = 0;
  do {
    if (i !== 0) {
      STY_ERROR("Invalid input. Only positive numbers are allowed. Try again.")(
        EOL
      );
    }
    STY_DEFAULT("Insert key length: ");
    const input = await textInput({ default: overwrite?.length.toString() });
    if (/^\+?[0-9]+$/.test(input)) {
      length = +input;
    }
  } while (length === 0 && ++i < 3);
  if (length === 0) {
    throw new CommandError("No schema length provided.");
  }
  const settings = getSettings();
  const items = settings.char_set_collections.map(
    (collection) => collection.name
  );
  items.push("Custom");
  const result = await singleColumnMenu(items);
  let characters;
  if (result.selectedIndex === items.length - 1) {
    const CUSTOM = settings.char_set_custom;
    const choices = (settings.char_sets as (CharSet | CustomCharSet)[]).concat([
      CUSTOM,
    ]);
    const PROMPT = {
      type: "checkbox",
      name: "sets",
      message: "Select character sets",
      choices: choices.map((choice, idx) => ({ ...choice, value: idx })),
    };
    terminal.grabInput(false);
    const promise = prompt([PROMPT]);
    const { sets } = (await promise) as { sets: number[] };
    // close function is marked protected but we need to call it anyways...
    ((promise.ui as unknown) as { close: () => void }).close();
    terminal.grabInput();
    characters = "";
    for (const idx of sets) {
      const charSet = choices[idx];
      if (isCustomCharSet(charSet)) {
        STY_DEFAULT("Insert custom characters: ");
        characters += await textInput();
      } else {
        // I don't get why charSet isn't correctly inferred by typescript...
        characters += (charSet as CharSet).characters;
      }
    }
  } else {
    characters = settings.char_set_collections[
      result.selectedIndex
    ].char_set_ids
      .map((idx) => settings.char_sets[idx].characters)
      .join("");
  }
  return { length, characters };
}

export async function queryStoredKey(methods: CipherMethod[]) {
  STY_DEFAULT("Insert content to store: ");
  const content = await textInput({ echo: false });
  const masterKey = getState().masterKey;
  return await encrypt(Buffer.from(content), methods, masterKey);
}

export async function queryMetaName() {
  STY_DEFAULT("Insert meta key: ");
  return await textInput();
}

export async function queryMetaValue(oldValue?: string) {
  STY_DEFAULT("Insert meta value: ");
  return await textInput({ default: oldValue });
}
