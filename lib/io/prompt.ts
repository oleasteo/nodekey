import { getSettings } from "../services/settings";
import { getState } from "../services/state";
import {
  STY_PROMPT_CARET,
  STY_PROMPT_CWD,
  STY_PROMPT_DEFAULT,
  STY_PROMPT_META_DEFAULT,
  STY_PROMPT_META_KEY,
  STY_PROMPT_META_VALUE,
  STY_PROMPT_NUM_APPS,
  STY_PROMPT_NUM_KEYS,
  STY_PROMPT_NUM_SHELLS,
} from "../constants/styles";
import { appFromPath } from "../services/path/application";
import { getRootApplicationsList } from "../services/profile";
import { EOL } from "os";

export function print() {
  /*
   * Prompt format:
   *
   * #SESSIONS <CWD>[<#APPS>,<#KEYS>] <app meta>
   * > <CURSOR>
   */
  const cfg = getSettings().output.prompt;
  const state = getState();
  STY_PROMPT_NUM_SHELLS.noFormat(state.depth);
  STY_PROMPT_DEFAULT(" ");
  STY_PROMPT_CWD(state.cwd.toString(true));
  STY_PROMPT_DEFAULT(" [");
  const app = appFromPath(state.cwd);
  const children = app === null ? getRootApplicationsList() : app.childrenList;
  const keys = app === null ? [] : app.keysList;
  STY_PROMPT_NUM_APPS.noFormat(children.length);
  STY_PROMPT_DEFAULT("|");
  STY_PROMPT_NUM_KEYS.noFormat(keys.length);
  STY_PROMPT_DEFAULT("]");
  if (cfg.meta && app !== null && app.meta.length > 0) {
    STY_PROMPT_DEFAULT(" ");
    for (let i = 0; i < app.meta.length; i++) {
      const current = app.meta[i];
      if (i !== 0) {
        STY_PROMPT_META_DEFAULT(", ");
      }
      STY_PROMPT_META_KEY(current.name);
      STY_PROMPT_META_DEFAULT(": ");
      STY_PROMPT_META_VALUE(current.value);
    }
  }
  STY_PROMPT_DEFAULT(EOL);
  STY_PROMPT_CARET("> ");
}
