import {
  STY_ERROR,
  STY_INTEGRITY_SUCCESS,
  STY_KEY_STORED,
} from "../constants/styles";
import { EOL } from "os";

/*---------------------------------- Errors ----------------------------------*/

export function errIntegrityFail() {
  STY_ERROR("Validation failed.")(EOL);
}

export function errAliasNotFound(name: string) {
  STY_ERROR("Alias not found: " + name)(EOL);
}

export function errPathAlreadyExists(path: string) {
  STY_ERROR("Path already exists: " + path)(EOL);
}

export function errPathNotFound(path: string) {
  STY_ERROR("Path not found: " + path)(EOL);
}

export function errRemoveRootFail() {
  STY_ERROR("Cannot (re)move root application.")(EOL);
}

export function errMetaNothing(path: string) {
  STY_ERROR("No meta data: " + path)(EOL);
}

export function errMetaNotFound(name: string) {
  STY_ERROR("Meta data not found: " + name)(EOL);
}

export function errBookmarkDeleteHomeFail() {
  STY_ERROR("Cannot delete HOME bookmark (~).")(EOL);
}

export function errBookmarkAlreadyExists(id: string) {
  STY_ERROR("Bookmark already exists (use '-f' to overwrite): " + id + ".")(
    EOL
  );
}

export function errBookmarkNotFound(id: string) {
  STY_ERROR("Bookmark not found: " + id)(EOL);
}

export function errKeysAtRoot() {
  STY_ERROR("Cannot create keys at root path.")(EOL);
}

export function errKeyCreateNoParent(path: string) {
  STY_ERROR("Cannot create key without parent: " + path)(EOL);
}

export function errKeyAlreadyExists(path: string) {
  STY_ERROR("Key already exists: " + path)(EOL);
}

export function errKeyNotFound(path: string) {
  STY_ERROR("Key not found: " + path)(EOL);
}

export function errKeyAlreadyDefault() {
  STY_ERROR("Key is already default.")(EOL);
}

export function errKeyDecryptFail(name: string) {
  STY_ERROR("Failed to decrypt ");
  STY_KEY_STORED(name)(EOL);
}

export function errKeyMoveNoForce() {
  STY_ERROR(
    "Use -f to force moving keys from one application to another. This might change the key content."
  )(EOL);
}

export function errApplicationNoDefault(path: string) {
  STY_ERROR("No default key: " + path)(EOL);
}

export function errApplicationNotSpecified() {
  STY_ERROR("No application specified.")(EOL);
}

export function errApplicationCreateNoParent(path: string) {
  STY_ERROR("Cannot create application without parent (try '-p'): " + path)(
    EOL
  );
}

export function errApplicationEditRoot() {
  STY_ERROR("Cannot edit root application.")(EOL);
}

export function errApplicationRemoveRecursive(path: string) {
  STY_ERROR(
    "Cannot remove application with children (use -r to remove recursive): " +
      path
  )(EOL);
}

export function errApplicationRemoveKeys(path: string) {
  STY_ERROR("Cannot remove application with keys (use -f to force): " + path)(
    EOL
  );
}

/*--------------------------------- Success  ---------------------------------*/

export function printIntegritySuccess() {
  STY_INTEGRITY_SUCCESS("Validation succeeded.")(EOL);
}
