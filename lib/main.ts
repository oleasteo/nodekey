#!/usr/bin/env node
import { mkdirp } from "fs-extra";
import parseArguments from "./services/arguments";
import {
  applyMatchingAlias,
  collectCommands,
  exec,
  execAll,
  execFn,
  getExecutor,
  parseCommand,
} from "./services/commands";
import { join, resolve } from "path";
import {
  getData,
  getKey,
  init as initProfile,
  read as readProfile,
} from "./services/profile";
import { EOL, homedir, type } from "os";
import { init as initSettings } from "./services/settings";
import { substituteUser } from "./commands/shell";
import { print as printPrompt } from "./io/prompt";
import {
  cancelInput,
  cleanUp,
  getCurrentInput,
  isCancelable,
  textInput,
} from "./io/terminal";
import { terminal } from "terminal-kit";
import { appFromPath, pathFromAppId } from "./services/path/application";
import Path, { ROOT } from "./services/path/Path";
import { getState, pushState } from "./services/state";
import {
  STY_CTRL,
  STY_DEFAULT,
  STY_ERROR,
  STY_INIT_PATHS,
} from "./constants/styles";
import { completeList } from "./services/completion";
import CommandError from "./types/CommandError";
import { CompletionResult } from "terminal-kit";

// eslint-disable-next-line @typescript-eslint/no-var-requires
const PACKAGE = require("../package.json");
const ETX = Buffer.from("03", "hex");
const EOT = Buffer.from("04", "hex");
const FF = Buffer.from("0C", "hex");

/*============================ Initial Execution  ============================*/

let inLoop = false;

init().catch(async (err: Error) => {
  STY_ERROR(err.stack)(EOL);
  await cleanUp(7);
});

/*================================ Functions  ================================*/

async function init() {
  const args = parseArguments();
  if (args.version) {
    STY_DEFAULT("NodeKEY v" + PACKAGE.version)(EOL);
    await cleanUp(0);
  }
  terminal.on("key", keyEvent);
  const isInteractive =
    args.interactive === true || !Array.isArray(args.execute);
  const profileDir = await getDir(args);
  STY_DEFAULT("Profile path: ");
  STY_INIT_PATHS(profileDir)(EOL);
  const settingsFile = await getSettingsFile(args, profileDir);
  await initSettings(settingsFile, isInteractive);
  // login and read profile
  await initProfile(profileDir);
  const profileResult = await execFn(
    () => readProfile(profileDir, args.master),
    []
  );
  if (!profileResult.success) {
    await cleanUp(2);
  }
  // go to home dir
  const bookmarks = getData().bookmarks;
  const homePath = Reflect.has(bookmarks, "~")
    ? pathFromAppId(bookmarks["~"])
    : ROOT;
  pushState({ masterKey: getKey() || Buffer.alloc(0), cwd: homePath });
  // execute arguments
  if (Array.isArray(args.execute) && !(await execAll(args.execute))) {
    await cleanUp(1);
  }
  // interactive mode
  if (isInteractive) {
    const key = getKey();
    const argv = key ? ["su", "-", ".", key.toString()] : ["su", "-"];
    await execFn(substituteUser, argv, argv.slice(1));
    await interactiveLoop();
  }
}

async function interactiveLoop() {
  inLoop = true;
  while (inLoop) {
    resolveCWD();
    printPrompt();
    let result: string | null = null;
    const history = getState().history;
    try {
      result = await textInput({
        cancelable: true,
        history: history,
        autoCompleteHint: true,
        autoCompleteMenu: true,
        async autoComplete(input: string) {
          return await autoCompletion(input);
        },
      });
    } catch (err) {
      if (err.message === "cancelled") {
        continue;
      }
    }
    if (result) {
      result = result.trim();
      if (result !== history[history.length - 1]) {
        history.push(result);
      }
      if (history.length >= 1020) {
        getState().history = history.slice(history.length - 1000);
      }
      await exec(parseCommand(result));
    }
  }
}

function resolveCWD() {
  let success = false;
  while (!success) {
    const cwd = getState().cwd;
    try {
      appFromPath(cwd);
      success = true;
    } catch (err) {
      if (
        err instanceof CommandError &&
        err.message.startsWith("No such application:")
      ) {
        cwd.append(Path.fromString(".."));
      } else {
        throw err;
      }
    }
  }
}

async function autoCompletion(input: string): Promise<CompletionResult> {
  const commands = parseCommand(input);
  const argv = commands.length > 0 ? commands[commands.length - 1] : null;
  let result: CompletionResult;
  if (
    argv !== null &&
    (argv.length > 1 || (argv.length === 1 && argv.trailingSpace))
  ) {
    applyMatchingAlias(argv);
    const cmdName = argv[0];
    const executor = getExecutor(cmdName);
    if (executor == null || typeof executor.complete !== "function") {
      return input;
    }
    const parameters = argv.slice(
      1,
      argv.trailingSpace ? argv.length : argv.length - 1
    );
    const currentParameter = argv.trailingSpace ? "" : argv[argv.length - 1];
    const _len = parameters.length;
    result = executor.complete(
      currentParameter,
      parameters,
      argv.trailingSpace ? _len : _len - 1
    );
    if (typeof result === "string") {
      return input + result;
    }
  } else {
    result = completeList((argv && argv[0]) || "", collectCommands());
    if (typeof result === "string") {
      return input + result + " ";
    }
    result.postfix = (result.postfix || "") + " ";
  }
  if (result._truncate) {
    input = input.substring(0, input.length - result._truncate);
  }
  result.prefix = input + (result.prefix || "");
  return result;
}

async function getDir(args: Record<string, unknown>): Promise<string> {
  if (typeof args.path === "string") {
    return resolve(__dirname, "..", args.path);
  }
  if (["Linux", "Darwin"].includes(type())) {
    const home = join(homedir(), ".config", "NodeKEY");
    await mkdirp(home);
    return home;
  }
  const dir = resolve(__dirname, "..", "user_data");
  await mkdirp(dir);
  return dir;
}

async function getSettingsFile(
  args: Record<string, unknown>,
  profileDir: string
): Promise<string> {
  if (typeof args.settings === "string") {
    const file = resolve(__dirname, "..", args.settings);
    STY_DEFAULT("Using settings from: ");
    STY_INIT_PATHS(file)(EOL);
    return file;
  }
  return join(profileDir, "settings.json");
}

async function keyEvent(
  name: string,
  matches: string[],
  data: { isCharacter: boolean; code?: Buffer }
) {
  if (data.code instanceof Buffer) {
    if (!isCancelable()) {
      return;
    }
    if (ETX.equals(data.code)) {
      STY_CTRL.noFormat("^C")(EOL);
      cancelInput();
      return;
    }
    if (EOT.equals(data.code) && !getCurrentInput()) {
      STY_CTRL.noFormat("^D")(EOL);
      await exec([["exit"]]);
      cancelInput();
      return;
    }
    if (inLoop && FF.equals(data.code)) {
      if (!isCancelable()) {
        return;
      }
      if (getCurrentInput()) {
        return;
      }
      cancelInput();
      exec([["clear"]]);
      return;
    }
  }
}
