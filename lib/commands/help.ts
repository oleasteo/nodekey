import { CommandExecutor } from "../types/CommandExecutor";
import CommandError from "../types/CommandError";
import { printHelpList } from "../io/print";
import { printHelp } from "../io/help";

const CMD_HELP = {
  help: {
    head: "[CMD]",
    description:
      "Displays available commands. If CMD is specified, displays the description of that command instead.",
  },
  cmd: "help",
  callback: help,
} as CommandExecutor;

export default [CMD_HELP];

function help(
  parameters: string[],
  argv: string[],
  executors: CommandExecutor[]
) {
  if (parameters.length > 0) {
    let some = false;
    for (const executor of executors) {
      if (parameters.includes(executor.cmd)) {
        printHelp(executor);
        some = true;
      }
    }
    if (!some) {
      throw new CommandError("Unknown command(s): " + parameters.join(","));
    }
    return;
  }
  printHelpList(executors);
}
