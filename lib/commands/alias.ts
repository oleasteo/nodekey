import { CommandExecutor } from "../types/CommandExecutor";
import { getData, persist } from "../services/profile";
import CommandError from "../types/CommandError";
import { printAlias } from "../io/print";
import { completeList } from "../services/completion";
import { errAliasNotFound } from "../io/feedback";
import { genProfile } from "../constants/defaultProfile";

const CMD_ALIAS = {
  help: {
    head: "ALIAS REPLACE... | -d ALIAS... | -l | -r",
    description: "Create/Delete/List/Reset command aliases.",
  },
  cmd: "alias",
  callback: alias,
  complete,
} as CommandExecutor;

export default [CMD_ALIAS];

function complete(currentParam: string, parameters: string[]) {
  if (currentParam.length > 0 && currentParam[0] === "-") {
    return "";
  }
  if (parameters[0] !== "-d") {
    return "";
  }
  return completeList(currentParam, Object.keys(getData().aliases));
}

async function alias(parameters: string[]) {
  const profile = getData(),
    aliases = profile.aliases;
  if (parameters[0] === "-r") {
    profile.aliases = genProfile().aliases;
    await persist();
  } else if (parameters[0] === "-d") {
    let pendingPersist = false;
    for (let i = 1; i < parameters.length; i++) {
      const name = parameters[i];
      if (!Reflect.has(aliases, name)) {
        errAliasNotFound(name);
        break;
      }
      Reflect.deleteProperty(aliases, name);
      pendingPersist = true;
    }
    pendingPersist && (await persist());
  } else if (parameters[0] === "-l") {
    for (const key in aliases) {
      printAlias(key, aliases[key]);
    }
  } else {
    if (parameters.length < 2) {
      throw new CommandError("No command specified.");
    }
    if (/\s/.test(parameters[0])) {
      throw new CommandError("Alias may not contain whitespace.");
    }
    aliases[parameters[0]] = parameters.slice(1).join(" ");
    await persist();
  }
}
