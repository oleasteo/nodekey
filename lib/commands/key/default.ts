import { CommandExecutor } from "../../types/CommandExecutor";
import Path from "../../services/path/Path";
import { appFromPath, readStats } from "../../services/path/application";
import { getState } from "../../services/state";
import { persist } from "../../services/profile";
import { parseFlags } from "../../services/util";
import { Application } from "../../types/Application";
import CommandError from "../../types/CommandError";
import { errKeyAlreadyDefault, errKeyNotFound } from "../../io/feedback";
import { completeApp, completeKey } from "../../services/completion";

export default {
  help: {
    head: "KEY... | -d APP",
    description:
      "Make key(s) the default of its/their application(s). Use -d option to unset the default of an " +
      "application.",
    see: ["fetch"],
  },
  cmd: "default",
  callback: mkDefault,
  complete,
} as CommandExecutor;

function complete(currentParam: string, parameters: string[], idx: number) {
  if (currentParam.length > 0 && currentParam[0] === "-") {
    return "";
  }
  const flags = parseFlags(parameters);
  if (Reflect.has(flags.map, "d")) {
    if (
      flags.remainder.length === 0 ||
      (flags.remainder.length === 1 && idx < parameters.length)
    ) {
      return completeApp(currentParam);
    }
    return "";
  }
  return completeKey(currentParam);
}

async function mkDefault(parameters: string[]) {
  const flags = parseFlags(parameters, { d: 1 });
  let pendingPersist = false;
  if (Reflect.has(flags.map, "d")) {
    const appPath = flags.map["d"].argv[0];
    const app = appFromPath(
      Path.join(getState().cwd, Path.fromString(appPath))
    );
    if (app === null) {
      throw new CommandError("Cannot unset default of /.");
    }
    if (app.defaultKeyId === null) {
      throw new CommandError("Application " + app.name + " has no default.");
    }
    app.defaultKeyId = null;
    pendingPersist = true;
  } else {
    const cwd = appFromPath(getState().cwd);
    for (const param of flags.remainder) {
      const stats = readStats(Path.fromString(param), cwd);
      if (stats.key === null) {
        errKeyNotFound(param);
        break;
      }
      // as key is non-null, app must be non-null as well
      const app = stats.app as Application;
      if (stats.key._id === app.defaultKeyId) {
        errKeyAlreadyDefault();
        break;
      }
      app.defaultKeyId = stats.key._id;
      pendingPersist = true;
    }
  }
  pendingPersist && persist();
}
