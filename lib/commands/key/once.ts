import { CommandExecutor } from "../../types/CommandExecutor";
import { parseFlags } from "../../services/util";
import { randomBytes } from "crypto";
import { appFromPath, pathFromApp } from "../../services/path/application";
import Path from "../../services/path/Path";
import { getState } from "../../services/state";
import { KeySchema } from "../../types/Application";
import { fromSchema } from "../../services/crypto/hashing";
import { getSettings } from "../../services/settings";
import { hashKey } from "../../services/crypto/index";
import CommandError from "../../types/CommandError";
import { printOnceSalt } from "../../io/print";
import { completeApp } from "../../services/completion";
import { copyKeyContentToClipboard } from "./getter";

export default {
  help: {
    head: "[APP | LENGTH CHARSET] [-s SALT]",
    description:
      "Generate a new key. The new key will be copied according to fetch settings and displayed. The " +
      "schema is defined by the specified APP or LENGTH and CHARSET.",
    see: ["key"],
  },
  cmd: "once",
  callback: once,
  complete,
} as CommandExecutor;

function complete(currentParam: string, parameters: string[], idx: number) {
  if (currentParam.length > 0 && currentParam[0] === "-") {
    return "";
  }
  const flags = parseFlags(parameters, { s: 1 });
  if (
    Reflect.has(flags.map, "i") ||
    (Reflect.has(flags.map, "s") && flags.map["s"].argv.length === 0)
  ) {
    return "";
  }
  if (
    flags.remainder.length === 0 ||
    (flags.remainder.length === 1 && idx < parameters.length)
  ) {
    return completeApp(currentParam);
  }
  return "";
}

async function once(parameters: string[]) {
  const flags = parseFlags(parameters, { s: 1 });
  let schema: KeySchema;
  let salt: Buffer;
  if (Reflect.has(flags.map, "s") && flags.map["s"].argv.length === 1) {
    salt = Buffer.from(flags.map["s"].argv[0], "hex");
  } else {
    salt = randomBytes(8);
  }
  if (flags.remainder.length === 1) {
    const app = appFromPath(
      Path.join(getState().cwd, Path.fromString(flags.remainder[0]))
    );
    if (app === null || app.schema === null) {
      throw new CommandError(
        pathFromApp(app).toString() + " has no schema to use."
      );
    }
    schema = app.schema;
  } else {
    if (!/^\+?[0-9]+$/.test(flags.remainder[0])) {
      throw new CommandError(`'${flags.remainder[0]}' is no valid length.`);
    }
    schema = { length: +flags.remainder[0], characters: flags.remainder[1] };
  }
  const methods = getSettings().hashing.map((schema) =>
    fromSchema(schema, salt)
  );
  const result = await hashKey(methods, schema, getState().masterKey);
  printOnceSalt(salt);
  await copyKeyContentToClipboard(result);
}
