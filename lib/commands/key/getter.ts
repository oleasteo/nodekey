import { CommandExecutor } from "../../types/CommandExecutor";
import Path from "../../services/path/Path";
import { appFromPath, readStats } from "../../services/path/application";
import { getState } from "../../services/state";
import { Key } from "../../types/Key";
import { decrypt, hashKey } from "../../services/crypto/index";
import { getApplications } from "../../services/profile";
import { CipherMethod } from "../../types/Method";
import { parseFlags } from "../../services/util";
import { clearClipboard, writeClipboard } from "../../services/clipboard";
import { getSettings } from "../../services/settings";
import {
  errApplicationNoDefault,
  errKeyDecryptFail,
  errKeyNotFound,
} from "../../io/feedback";
import { printKeyContent, printMetaLine } from "../../io/print";
import { completePath } from "../../services/completion";
import CommandError from "../../types/CommandError";

const CMD_CAT = {
  help: {
    head: "PATH...",
    description: "Print application and/or key data (meta data, key content).",
    see: ["fetch"],
  },
  cmd: "cat",
  callback: cat,
  complete: completeCat,
} as CommandExecutor;
const CMD_FETCH = {
  help: {
    head: "[-m|-M] [-p|-P] [-v|-V] PATH",
    description:
      "Fetch some key. The m/M option en/disables meta data print. The p/P option en/disables key print." +
      "The v/V option en/disables visibility of printed key (if disabled, the fg color is the same as bg color). " +
      "Defaults can be specified within settings.json. If the passed PATH is an application, its default key will " +
      "be used. TIMEOUT can be specified in seconds.",
    see: ["cat", "default"],
  },
  cmd: "fetch",
  callback: fetch,
  complete: completeFetch,
} as CommandExecutor;

export default [CMD_CAT, CMD_FETCH];

function completeCat(currentParam: string) {
  return completePath(currentParam);
}

function completeFetch(
  currentParam: string,
  parameters: string[],
  idx: number
) {
  if (currentParam.length > 0 && currentParam[0] === "-") {
    return "";
  }
  const flags = parseFlags(parameters);
  if (
    flags.remainder.length === 0 ||
    (flags.remainder.length === 1 && idx < parameters.length)
  ) {
    return completePath(currentParam);
  }
  return "";
}

async function cat(parameters: string[]) {
  const cwd = appFromPath(getState().cwd);
  for (const param of parameters) {
    const stats = readStats(Path.fromString(param), cwd);
    const key = stats.key;
    if (key === null) {
      if (stats.isApp) {
        errApplicationNoDefault(param);
      } else {
        errKeyNotFound(param);
      }
      break;
    }
    await catKeyContent(key);
  }
}

export async function catKeyContent(key: Key) {
  const content = await getKeyContent(key);
  if (content === null) {
    return errKeyDecryptFail(key.name);
  }
  printKeyContent(content, false);
}

async function fetch(parameters: string[]) {
  const flags = parseFlags(parameters);
  const param = flags.remainder[flags.remainder.length - 1];
  const stats = readStats(Path.fromString(param));
  const key = stats.key;
  if (key === null) {
    if (stats.isApp) {
      errApplicationNoDefault(param);
    } else {
      errKeyNotFound(param);
    }
    return;
  }
  const meta = Reflect.has(flags.map, "M")
    ? false
    : Reflect.has(flags.map, "m")
    ? true
    : undefined;
  const print = Reflect.has(flags.map, "P")
    ? false
    : Reflect.has(flags.map, "p")
    ? true
    : undefined;
  const visible = Reflect.has(flags.map, "V")
    ? false
    : Reflect.has(flags.map, "v")
    ? true
    : undefined;
  await fetchKeyContent(key, meta, print, visible);
}

export async function fetchKeyContent(
  key: Key,
  meta?: boolean,
  print?: boolean,
  visible?: boolean
) {
  const content = await getKeyContent(key);
  if (content === null) {
    errKeyDecryptFail(key.name);
    return;
  }
  const cfg = getSettings().output.fetch;
  if (
    key.meta.length > 0 &&
    ((cfg.meta && meta !== false) || (!cfg.meta && meta === true))
  ) {
    printMetaLine(key.meta);
  }
  if ((cfg.print && print !== false) || (!cfg.print && print === true)) {
    const hidden =
      (cfg.hidden && visible !== true) || (!cfg.hidden && visible === false);
    printKeyContent(content, hidden);
  }
  copyKeyContentToClipboard(content);
}

export async function copyKeyContentToClipboard(content: Buffer) {
  const cfg = getSettings().output.fetch.clipboards[0];
  await writeClipboard(content);
  if (cfg !== undefined && cfg.timeout !== null && cfg.timeout > 0) {
    setTimeout(() => clearClipboard(content), cfg.timeout);
  }
}

async function getKeyContent(key: Key): Promise<Buffer | null> {
  const app = getApplications()[key._appId];
  const masterKey = getState().masterKey;
  if (key.hashed) {
    if (app.schema === null) {
      throw new CommandError(
        "Application schema must be set if any hashed keys exist."
      );
    }
    return await hashKey(key.methods, app.schema, masterKey);
  } else {
    return await decrypt(
      key.value as Buffer,
      key.methods as CipherMethod[],
      masterKey
    );
  }
}
