import { CommandExecutor } from "../../types/CommandExecutor";
import { parseFlags } from "../../services/util";
import { appFromPath, readStats } from "../../services/path/application";
import { getState } from "../../services/state";
import { persist } from "../../services/profile";
import Path from "../../services/path/Path";
import { Key } from "../../types/Key";
import { fromSchema as fromHashingSchema } from "../../services/crypto/hashing";
import { getSettings } from "../../services/settings";
import { fromSchema as fromEncryptionSchema } from "../../services/crypto/encryption";
import { v4 as uuid } from "uuid";
import {
  errKeyAlreadyExists,
  errKeyCreateNoParent,
  errKeysAtRoot,
  errPathAlreadyExists,
} from "../../io/feedback";
import { queryStoredKey } from "../../io/query";
import { completeApp } from "../../services/completion";
import { Application } from "../../types/Application";
import { catKeyContent, fetchKeyContent } from "./getter";

export default {
  help: {
    head: "[-s] [-f] [-c] PATH...",
    description:
      "Creates a new application key. If '-s' is passed, the key content will be encrypted instead of " +
      "generated. Generated keys only store the construction schema and a salt. In combination with the master " +
      "key, the generated content is produced by hashing methods each time it is fetched. This is the recommended " +
      "method to save application keys as you gain maximum entropy for each application key. If an application is " +
      "specified, the key will be named generically and if it's the first such key, it will be the default key for " +
      "the application. The '-f' and '-c' flags will fetch/cat the created key respectively.",
    see: ["once", "fetch", "default"],
  },
  cmd: "key",
  callback: create,
  complete,
} as CommandExecutor;

function complete(currentParam: string) {
  if (currentParam.length > 0 && currentParam[0] === "-") {
    return "";
  }
  return completeApp(currentParam);
}

async function create(parameters: string[]) {
  const flags = parseFlags(parameters);
  const isStored = Reflect.has(flags.map, "s");
  const cwd = appFromPath(getState().cwd);
  let pendingPersist = false;
  for (const param of flags.remainder) {
    const stats = readStats(Path.fromString(param), cwd);
    if (stats.isResolved && stats.isKey) {
      errPathAlreadyExists(param);
      break;
    }
    const app = stats.app;
    if (app === null) {
      errKeysAtRoot();
      break;
    }
    const path = stats.relPath;
    if (path && path.reverseParts.length > 1) {
      errKeyCreateNoParent(param);
      break;
    }
    const hasName = path !== null && path.reverseParts.length === 1;
    const key = await createKeyFor(
      app,
      !isStored,
      undefined,
      hasName ? (path as Path).reverseParts[0] : undefined
    );
    if (key !== undefined) {
      pendingPersist = true;
      if (Reflect.has(flags.map, "c")) {
        await catKeyContent(key);
      }
      if (Reflect.has(flags.map, "f")) {
        await fetchKeyContent(key);
      }
    }
  }
  pendingPersist && (await persist());
}

export async function createKeyFor(
  app: Application,
  hashed: boolean,
  cdate: number = Date.now(),
  name?: string
): Promise<Key | void> {
  const defaultCandidate = name === undefined;
  if (name === undefined) {
    name = cdate + "";
  }
  if (Reflect.has(app.keys, name)) {
    return errKeyAlreadyExists(name);
  }
  const key: Key = {
    _appId: app._id,
    _id: uuid(),
    cdate,
    name: name || cdate + "",
    meta: [],
    ...(await _createBase(hashed)),
  };
  app.keys[key.name] = key;
  app.keysList.push(key);
  if (defaultCandidate && app.defaultKeyId === null) {
    app.defaultKeyId = key._id;
  }
  return key;
}

async function _createBase(hashed: boolean) {
  const settings = getSettings();
  if (hashed) {
    return {
      hashed,
      methods: settings.hashing.map((schema) => fromHashingSchema(schema)),
    };
  } else {
    const methods = settings.encryption.map((schema) =>
      fromEncryptionSchema(schema)
    );
    return {
      hashed,
      methods: methods,
      value: await queryStoredKey(methods),
    };
  }
}
