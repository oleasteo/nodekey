import CMD_CREATE from "./create";
import CMD_DEFAULT from "./default";
import CMD_GETTER from "./getter";
import CMD_ONCE from "./once";

export default CMD_GETTER.concat([CMD_DEFAULT, CMD_CREATE, CMD_ONCE]);
