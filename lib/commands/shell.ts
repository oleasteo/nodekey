import { CommandExecutor } from "../types/CommandExecutor";
import {
  dropState,
  getState,
  PushState,
  pushState,
  putState,
} from "../services/state";
import { cleanUp } from "../io/terminal";
import { terminal } from "terminal-kit";
import Path from "../services/path/Path";
import { queryMasterKey } from "../io/query";
import { completePath } from "../services/completion";
import { parseFlags } from "../services/util";
import { clearClipboard } from "../services/clipboard";

const CMD_SH = {
  help: {
    head: "[PATH=.]",
    description: "Creates a session clone.",
    see: ["exit", "su"],
  },
  cmd: "sh",
  callback: shell,
  complete: completeSh,
} as CommandExecutor;
const CMD_SU = {
  help: {
    head: "[-] [PATH=.] [KEY]",
    description:
      "Creates a new login session. Iff the first parameter is '-', the current session will be " +
      "overwritten.",
    see: ["integrity", "sh"],
  },
  cmd: "su",
  callback: substituteUser,
  complete: completeSu,
} as CommandExecutor;
const CMD_CLEAR = {
  help: {
    head: "[-c] [-S]",
    description:
      "Clear the screen unless '-S' is passed. If '-c' is passed, in addition clear all clipboards.",
  },
  cmd: "clear",
  callback: clear,
} as CommandExecutor;
const CMD_EXIT = {
  help: {
    description: "Exit the current session.",
    see: ["sh", "su"],
  },
  cmd: "exit",
  callback: exit,
} as CommandExecutor;

export default [CMD_SH, CMD_SU, CMD_CLEAR, CMD_EXIT];

function completeSh(currentParam: string, parameters: string[], idx: number) {
  if (
    parameters.length === 0 ||
    (parameters.length === 1 && idx < parameters.length)
  ) {
    return completePath(currentParam);
  }
  return "";
}

function completeSu(currentParam: string, parameters: string[], idx: number) {
  if (currentParam.length > 0 && currentParam[0] === "-") {
    return "";
  }
  const flags = parseFlags(parameters);
  if (
    flags.remainder.length === 0 ||
    (flags.remainder.length === 1 && idx < parameters.length)
  ) {
    return completePath(currentParam);
  }
  return "";
}

function shell(parameters: string[]) {
  const currentCWD = getState().cwd;
  const cwd =
    parameters.length > 0
      ? Path.join(currentCWD, Path.fromString(parameters[0]))
      : currentCWD.clone();
  pushState({ cwd, masterKey: getState().masterKey });
}

export async function substituteUser(parameters: string[]) {
  let idx = 0;
  let dropCurrent = false;
  let path = "";
  let key: Buffer | null = null;
  if (parameters.length > idx && parameters[idx] === "-") {
    dropCurrent = true;
    idx++;
  }
  if (parameters.length > idx) {
    path = parameters[idx++];
  }
  if (parameters.length > idx) {
    key = Buffer.from(parameters[idx++]);
  } else {
    key = await queryMasterKey();
  }
  if (key === null) {
    key = Buffer.from("todo");
  }
  const currentCWD = getState().cwd;
  const cwd =
    path.length > 0
      ? Path.join(currentCWD, Path.fromString(path))
      : currentCWD.clone();
  const state: PushState = { cwd, masterKey: key };
  if (dropCurrent) {
    putState(state);
  } else {
    pushState(state);
  }
}

async function exit() {
  clearClipboard(null);
  if (dropState() === null) {
    await cleanUp(0);
  }
}

function clear(parameters: string[]) {
  const flags = parseFlags(parameters);
  if (Reflect.has(flags.map, "c")) {
    clearClipboard(null);
  }
  if (!Reflect.has(flags.map, "S")) {
    terminal.clear();
  }
}
