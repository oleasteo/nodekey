import { CommandExecutor } from "../../types/CommandExecutor";
import * as path from "path";
import { homedir } from "os";
import { createWriteStream, statSync } from "fs";
import { parseFlags } from "../../services/util";
import { exportTXT } from "./export.txt";
import { exportCSV } from "./export.csv";

const CMD_EXPORT = {
  help: {
    head: "EXPORT [-c [-h]|-a] [FS_PATH=./nodekey-export.txt]",
    description:
      "Export the profile content. Unless the -c option is specified, the export type is TXT (complete " +
      "NodeKEY content). With the -c option, the export type will be CSV (keys and meta only); Some commonly used " +
      "meta keys are used to deduct the website, username and email information. The remaining unused meta will be " +
      "added as JSON or line-based human readable (-h option). The -a option enables plaintext export of all keys " +
      "(it is already implied for CSV exports); This might take some time.",
  },
  cmd: "export",
  callback: exportCMD,
} as CommandExecutor;

export const CURRENT_FORMAT_VERSION = 1;
export default [CMD_EXPORT];

async function exportCMD(parameters: string[]) {
  const flags = parseFlags(parameters);
  const csvExport = Reflect.has(flags.map, "c");

  let fsPath = flags.remainder[0] || "./";
  fsPath = fsPath.replace(/^~\//, homedir() + "/");
  fsPath = path.resolve(fsPath);
  if (statSync(fsPath).isDirectory()) {
    fsPath = path.join(
      fsPath,
      `nodekey-export.${Date.now()}.${csvExport ? "csv" : "txt"}`
    );
  }

  const ws = createWriteStream(fsPath, { mode: 0o600 });

  if (csvExport) {
    await exportCSV(ws, flags);
  } else {
    await exportTXT(ws, flags);
  }

  const endPromise = new Promise((resolve, reject) => {
    ws.once("error", reject);
    ws.once("finish", resolve);
  });
  ws.end();
  await endPromise;
}
