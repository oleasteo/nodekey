import { WriteStream } from "fs";
import { Application } from "../../types/Application";
import { EOL } from "os";
import { isHashKey, Key } from "../../types/Key";
import {
  CipherMethod,
  HashMethod,
  cipherMethodToDTO,
  hashMethodToDTO,
} from "../../types/Method";
import { Meta } from "../../types/Meta";
import {
  getApplications,
  getData,
  getRootApplicationsList,
} from "../../services/profile";
import { pathFromApp } from "../../services/path/application";
import { integrityToDTO } from "../../types/Profile";
import { CURRENT_FORMAT_VERSION } from "./export";
import { Flags } from "../../services/util";
import { getKeyContentPromises, KeyContentPromises } from "./utils";

export async function exportTXT(ws: WriteStream, flags: Flags) {
  const profile = getData();
  const rootApps = getRootApplicationsList();
  const apps = getApplications();
  // collect key value promises first for them to be computed in parallel
  const keyContentPromises = Reflect.has(flags.map, "a")
    ? getKeyContentPromises()
    : void 0;

  await ws.write(`# NodeKEY Export Format v${CURRENT_FORMAT_VERSION}${EOL}`);
  await ws.write(`${EOL}## Aliases${EOL}${EOL}`);
  for (const id in profile.aliases) {
    ws.write(`${JSON.stringify({ [id]: profile.aliases[id] })}${EOL}`);
  }
  await ws.write(`${EOL}## Bookmarks${EOL}${EOL}`);
  for (const id in profile.bookmarks) {
    const appId = profile.bookmarks[id];
    const appPath = pathFromApp(appId ? apps[appId] : null);
    ws.write(`${JSON.stringify({ [id]: appPath.toString(true) })}${EOL}`);
  }
  await ws.write(`${EOL}## Integrity${EOL}${EOL}`);
  for (const id in profile.integrity) {
    const integrity = integrityToDTO(profile.integrity[id]);
    ws.write(`${JSON.stringify({ [id]: integrity })}${EOL}`);
  }
  await ws.write(`${EOL}## Content${EOL}${EOL}`);
  for (const app of rootApps) {
    await writeApplicationRecursive(ws, app, keyContentPromises);
    ws.write(EOL);
  }
}

async function writeApplicationRecursive(
  ws: WriteStream,
  app: Application,
  keyContentPromises?: KeyContentPromises,
  prefix = "/"
) {
  // app path
  const appPath = `${prefix}${app.name}/`;
  ws.write(`${appPath}${EOL}`);

  // app information
  ws.write(`  ; {"cdate":${app.cdate}`);
  if (app.schema) {
    ws.write(
      `,"schema":{"length":${app.schema.length},"characters":${JSON.stringify(
        app.schema.characters
      )}}`
    );
  }
  ws.write(`}${EOL}`);

  // app user meta
  writeMeta(ws, app.meta, "  ; ");

  // keys
  for (const key of app.keysList) {
    await writeKey(ws, app, key, keyContentPromises);
  }

  // children
  if (app.childrenList.length) {
    ws.write(EOL);
  }
  for (let i = 0; i < app.childrenList.length; i++) {
    const child = app.childrenList[i];
    await writeApplicationRecursive(ws, child, keyContentPromises, appPath);
    if (i < app.childrenList.length - 1) {
      ws.write(EOL);
    }
  }
}

async function writeKey(
  ws: WriteStream,
  app: Application,
  key: Key,
  keyContentPromises?: KeyContentPromises
) {
  ws.write(`  ${key._id === app.defaultKeyId ? "*" : "+"} ${key.name}`);
  if (keyContentPromises) {
    const content = await keyContentPromises[key._id];
    if (content) {
      ws.write(` ; ${content} ;`);
    }
  }
  ws.write(EOL);

  const methods = key.methods;
  ws.write(
    `    ; {"cdate":${key.cdate},"hashed":${JSON.stringify(key.hashed)}`
  );
  if (!isHashKey(key)) {
    ws.write(`,"value":"${key.value.toString("base64")}"`);
  }
  ws.write(
    `,"methods":${JSON.stringify(
      key.hashed
        ? (methods as HashMethod[]).map(hashMethodToDTO)
        : (methods as CipherMethod[]).map(cipherMethodToDTO)
    )}`
  );
  ws.write(EOL);

  writeMeta(ws, key.meta, "    ; ");
}

function writeMeta(ws: WriteStream, meta: Meta[], prefix: string) {
  if (!meta.length) {
    return;
  }
  const collected: Record<string, string> = {};
  for (const m of meta) {
    collected[m.name] = m.value;
  }
  ws.write(`${prefix}${JSON.stringify(collected)}${EOL}`);
}
