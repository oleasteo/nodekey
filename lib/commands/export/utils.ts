import { Application } from "../../types/Application";
import { Key } from "../../types/Key";
import { getState } from "../../services/state";
import { decrypt, hashKey } from "../../services/crypto";
import { CipherMethod } from "../../types/Method";
import { flatMap, mapKeys, mapValues } from "lodash";
import { getApplications } from "../../services/profile";

export interface KeyContentPromises {
  [KeyId: string]: Promise<Buffer | null>;
}

export function getKeysList(apps = getApplications()): Key[] {
  return flatMap(apps, "keysList");
}

/**
 * Collect key value promises. This way they are computed in parallel.
 * @param apps The applications mapping.
 * @param keys List of all keys.
 * @returns A mapping of key ID to the Promise of its content.
 */
export function getKeyContentPromises(
  apps = getApplications(),
  keys = getKeysList(apps)
) {
  return mapValues(mapKeys(keys, "_id"), (key) =>
    getKeyContent(apps[key._appId], key)
  );
}

async function getKeyContent(
  app: Application,
  key: Key
): Promise<Buffer | null> {
  const masterKey = getState().masterKey;
  if (key.hashed) {
    if (app.schema === null) {
      return null;
    }
    return await hashKey(key.methods, app.schema, masterKey);
  } else {
    return await decrypt(
      key.value as Buffer,
      key.methods as CipherMethod[],
      masterKey
    );
  }
}
