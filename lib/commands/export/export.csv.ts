import { WriteStream } from "fs";
import { getApplications } from "../../services/profile";
import { getKeyContentPromises, getKeysList } from "./utils";
import { EOL } from "os";
import { pathFromApp } from "../../services/path/application";
import { Key } from "../../types/Key";
import { Application } from "../../types/Application";
import { Meta } from "../../types/Meta";
import { map, sortBy } from "lodash";
import { Flags } from "../../services/util";
import Path from "../../services/path/Path";

type Dictionary<T> = { [key: string]: T };

type KeyWithPath = Key & {
  id: string;
  path: Path;
};

interface DeepMeta {
  [Path: string]: Meta[];
}

export async function exportCSV(ws: WriteStream, flags: Flags) {
  const jsonMeta = !Reflect.has(flags.map, "h");
  const apps = getApplications();
  const keysWithPaths = sortBy(
    getKeysList(apps).map(
      (key): KeyWithPath => {
        const path = pathFromApp(apps[key._appId], apps).append(
          Path.fromString(key.name, false)
        );
        return {
          ...key,
          id: path.toString(),
          path: path,
        };
      }
    ),
    "id"
  );
  const keyContentPromises = getKeyContentPromises(apps, keysWithPaths);
  // UTF-8 BOM
  ws.write(new Uint8Array([0xef, 0xbb, 0xbf]));
  // title line
  ws.write(`Path,Password,Username,Email,Website,Metadata${EOL}`);

  const websiteKeys = listToHashObj(
    ["website", "url", "site", "web", "server", "domain"],
    true
  );
  const usernameKeys = listToHashObj(
    ["username", "user", "name", "account", "number", "id"],
    true
  );
  const emailKeys = listToHashObj(["email", "mail"], true);

  for (const key of keysWithPaths) {
    const password = await keyContentPromises[key._id];
    const excludeMetaKeys: Dictionary<Path> = {};
    const username = findMetaDeep(key, apps, usernameKeys, excludeMetaKeys);
    const email = findMetaDeep(key, apps, emailKeys, excludeMetaKeys);
    const website = findMetaDeep(key, apps, websiteKeys, excludeMetaKeys);
    ws.write(
      csvEntries(key.path.toString(), password, username, email, website)
    );
    const deepMeta = collectMetaDeep(key, apps, excludeMetaKeys);
    ws.write(",");
    if (deepMeta != null) {
      if (jsonMeta) {
        ws.write(csvEscape(JSON.stringify(deepMeta)));
      } else {
        ws.write(
          csvEscape(
            map(deepMeta, (metaList, path) => {
              const metaStr = metaList.map(
                ({ name, value }) => `${name} :: ${value}`
              );
              return `${path}${EOL}  ${metaStr}`;
            }).join(EOL)
          )
        );
      }
    }
    ws.write(EOL);
  }
}

function csvEntries(...values: (string | Buffer | null)[]) {
  return values.map(csvEscape).join(",");
}

function csvEscape(str: string | Buffer | null) {
  return str == null ? "" : `"${str.toString().replace(/"/g, `""`)}"`;
}

function collectMetaDeep(
  key: KeyWithPath,
  apps: { [key: string]: Application },
  excludeFieldsHashObj: Dictionary<Path>
): DeepMeta | null {
  const result: DeepMeta = {};
  let path = key.path;
  let meta: Meta[] | null = key.meta;
  let nextApp: Application | null = apps[key._appId];
  do {
    for (const m of meta) {
      const excludeOnPath = excludeFieldsHashObj[m.name.toLowerCase()];
      if (!excludeOnPath || !excludeOnPath.equals(path)) {
        const pathName = path.toString();
        if (Reflect.has(result, pathName)) {
          result[pathName].push(m);
        } else {
          result[pathName] = [m];
        }
      }
    }
    meta = nextApp?.meta ?? null;
    nextApp = nextApp?.parent ? apps[nextApp.parent] : null;
    path = path.parent();
  } while (meta != null);
  return Object.keys(result).length ? result : null;
}

function findMetaDeep(
  key: KeyWithPath,
  apps: { [key: string]: Application },
  fieldsHashObj: Dictionary<boolean>,
  targetFieldsHashObj: Dictionary<Path>
): string | null {
  let path = key.path;
  let meta: Meta[] | null = key.meta;
  let nextApp: Application | null = apps[key._appId];
  do {
    for (const m of meta) {
      const lowerName = m.name.toLowerCase();
      if (fieldsHashObj[lowerName]) {
        targetFieldsHashObj[lowerName] = path;
        return m.value;
      }
    }
    meta = nextApp?.meta ?? null;
    nextApp = nextApp?.parent ? apps[nextApp.parent] : null;
    path = path.parent();
  } while (meta != null);
  return null;
}

function listToHashObj<T>(keys: string[], value: T): Dictionary<T> {
  const obj: Record<string, T> = {};
  for (const key of keys) {
    obj[key] = value;
  }
  return obj;
}
