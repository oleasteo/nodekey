import { CommandExecutor } from "../../types/CommandExecutor";
import { completePath } from "../../services/completion";
import CommandError from "../../types/CommandError";
import { getState } from "../../services/state";
import Path from "../../services/path/Path";
import {
  appFromPath,
  PathStats,
  readStats,
} from "../../services/path/application";
import {
  errKeyAlreadyExists,
  errKeyMoveNoForce,
  errKeysAtRoot,
  errPathAlreadyExists,
  errPathNotFound,
  errRemoveRootFail,
} from "../../io/feedback";
import { Key } from "../../types/Key";
import { Application } from "../../types/Application";
import {
  getApplications,
  getRootApplications,
  getRootApplicationsList,
  persist,
} from "../../services/profile";
import { unregisterKey } from "./remove";
import { parseFlags } from "../../services/util";

export default {
  help: {
    head: "[-f] PATH... TARGET_PATH",
    description:
      "Move/Rename some application(s) or key(s). To move generated keys from one application to another, " +
      "the '-f' option must be specified since the key content changes if the application schemas differ.",
    see: ["default", "mkapp", "rm"],
  },
  cmd: "mv",
  callback: move,
  complete,
} as CommandExecutor;

function complete(currentParam: string) {
  if (currentParam.length > 0 && currentParam[0] === "-") {
    return "";
  }
  return completePath(currentParam);
}

async function move(parameters: string[]) {
  const flags = parseFlags(parameters);
  if (flags.remainder.length < 2) {
    throw new CommandError("No target specified.");
  }
  const cwd = appFromPath(getState().cwd);
  const targetParam = flags.remainder.pop() as string;
  const target = readStats(Path.fromString(targetParam), cwd);
  if (target.isKey) {
    return errKeyAlreadyExists(targetParam);
  }
  if (target.relPath !== null && target.relPath.reverseParts.length > 1) {
    return errPathNotFound(targetParam);
  }
  if (
    target.relPath !== null &&
    target.relPath.reverseParts.length === 1 &&
    flags.remainder.length > 2
  ) {
    return errPathNotFound(targetParam);
  }
  let pendingPersist = false;
  for (const param of flags.remainder) {
    const stats = readStats(Path.fromString(param), cwd);
    if (!stats.isResolved) {
      errPathNotFound(param);
      continue;
    }
    if (stats.isRoot) {
      errRemoveRootFail();
      continue;
    }
    if (stats.isKey) {
      if (target.isRoot) {
        errKeysAtRoot();
        continue;
      }
      if (
        (stats.key as Key).hashed &&
        target.app !== stats.app &&
        !Reflect.has(flags.map, "f")
      ) {
        errKeyMoveNoForce();
        continue;
      }
      if (!moveKey(stats.key as Key, target)) {
        continue;
      }
    } else {
      if (!moveApp(stats.app as Application, target)) {
        continue;
      }
    }
    pendingPersist = true;
  }
  pendingPersist && (await persist());
}

function moveKey(key: Key, target: PathStats): boolean {
  const app = target.app as Application;
  if (Reflect.has(app.keys, key.name) || Reflect.has(app.children, key.name)) {
    errKeyAlreadyExists(key.name);
    return false;
  }
  // unregister from old parent
  unregisterKey(key);
  // register to new parent
  key._appId = app._id;
  app.keysList.push(key);
  if (target.relPath !== null && target.relPath.reverseParts.length === 1) {
    key.name = target.relPath.reverseParts[0];
  }
  app.keys[key.name] = key;
  return true;
}

function moveApp(app: Application, target: PathStats): boolean {
  const newParent = target.app;
  const newParentChildren =
    newParent === null ? getRootApplications() : newParent.children;
  const newAppName =
    target.relPath !== null && target.relPath.reverseParts.length === 1
      ? target.relPath.reverseParts[0]
      : app.name;
  if (
    Reflect.has(newParentChildren, newAppName) ||
    (newParent !== null && Reflect.has(newParent.keys, newAppName))
  ) {
    errPathAlreadyExists(app.name);
    return false;
  }
  // unregister from old parent
  const oldParent = app.parent === null ? null : getApplications()[app.parent];
  const oldParentChildren =
    oldParent === null ? getRootApplications() : oldParent.children;
  const oldParentChildrenList =
    oldParent === null ? getRootApplicationsList() : oldParent.childrenList;
  oldParentChildrenList.splice(oldParentChildrenList.indexOf(app), 1);
  Reflect.deleteProperty(oldParentChildren, app.name);
  // register to new parent
  app.parent = newParent === null ? null : newParent._id;
  const newParentChildrenList =
    newParent === null ? getRootApplicationsList() : newParent.childrenList;
  newParentChildrenList.push(app);
  app.name = newAppName;
  newParentChildren[app.name] = app;
  return true;
}
