import { parseFlags } from "../../services/util";
import Path from "../../services/path/Path";
import { getState } from "../../services/state";
import {
  appFromPath,
  pathFromApp,
  readStats,
} from "../../services/path/application";
import {
  getApplications,
  getData,
  getRootApplications,
  getRootApplicationsList,
  persist,
} from "../../services/profile";
import { Application } from "../../types/Application";
import { CommandExecutor } from "../../types/CommandExecutor";
import {
  errApplicationRemoveKeys,
  errApplicationRemoveRecursive,
  errPathNotFound,
  errRemoveRootFail,
} from "../../io/feedback";
import { completePath } from "../../services/completion";
import { Key } from "../../types/Key";

export default {
  help: {
    head: "[-r] [-f] PATH...",
    description:
      "Remove some application or key. Use -f to force if application still contains keys. Use -r to " +
      "recursively remove if child apps exist.",
    see: ["mkapp", "mv"],
  },
  cmd: "rm",
  callback: remove,
  complete,
} as CommandExecutor;

function complete(currentParam: string) {
  if (currentParam.length > 0 && currentParam[0] === "-") {
    return "";
  }
  return completePath(currentParam);
}

async function remove(parameters: string[]) {
  const flags = parseFlags(parameters);
  const recursive = Reflect.has(flags.map, "r");
  const force = Reflect.has(flags.map, "f");
  let pendingPersist = false;
  const cwd = appFromPath(getState().cwd);
  for (const param of flags.remainder) {
    const stats = readStats(Path.fromString(param), cwd);
    if (!stats.isResolved) {
      errPathNotFound(param);
      continue;
    }
    if (stats.isRoot) {
      errRemoveRootFail();
      continue;
    }
    if (stats.isKey) {
      unregisterKey(stats.key as Key);
      pendingPersist = true;
    } else {
      if (!unregisterApp(stats.app as Application, recursive, force)) {
        break;
      }
      pendingPersist = true;
    }
  }
  if (pendingPersist) {
    // remove obsolete bookmarks
    const bookmarks = getData().bookmarks;
    const applications = getApplications();
    for (const key in bookmarks) {
      const appId = bookmarks[key];
      if (appId !== null && !Reflect.has(applications, appId)) {
        Reflect.deleteProperty(bookmarks, key);
      }
    }
    // persist
    await persist();
  }
}

export function unregisterKey(key: Key) {
  const parent = getApplications()[key._appId];
  parent.keysList.splice(parent.keysList.indexOf(key), 1);
  Reflect.deleteProperty(parent.keys, key.name);
  if (parent.defaultKeyId === key._id) {
    parent.defaultKeyId = null;
  }
}

function unregisterApp(
  app: Application,
  recursive: boolean,
  force: boolean
): boolean {
  let success = true;
  if (app.childrenList.length) {
    if (!recursive) {
      errApplicationRemoveRecursive(pathFromApp(app).toString(true));
      return false;
    }
    for (const child of app.childrenList) {
      if (!unregisterApp(child, recursive, force)) {
        success = false;
      }
    }
  }
  if (app.keysList.length) {
    if (!force) {
      errApplicationRemoveKeys(pathFromApp(app).toString(true));
      return false;
    }
  }
  if (success) {
    const applicationsList = getData().applications;
    const applications = getApplications();
    // remove global references
    Reflect.deleteProperty(applications, app._id);
    applicationsList.splice(applicationsList.indexOf(app), 1);
    // remove parental references
    let childrenList, children;
    if (app.parent === null) {
      childrenList = getRootApplicationsList();
      children = getRootApplications();
    } else {
      childrenList = applications[app.parent].childrenList;
      children = applications[app.parent].children;
    }
    Reflect.deleteProperty(children, app.name);
    childrenList.splice(childrenList.indexOf(app), 1);
  }
  return success;
}
