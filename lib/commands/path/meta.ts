import { CommandExecutor } from "../../types/CommandExecutor";
import { parseFlags } from "../../services/util";
import { pathFromApp, readStats } from "../../services/path/application";
import Path from "../../services/path/Path";
import { Meta } from "../../types/Meta";
import { Key } from "../../types/Key";
import { Application } from "../../types/Application";
import { STY_APP, STY_DEFAULT } from "../../constants/styles";
import { EOL } from "os";
import { persist } from "../../services/profile";
import CommandError from "../../types/CommandError";
import {
  errMetaNotFound,
  errMetaNothing,
  errPathNotFound,
} from "../../io/feedback";
import { printDefaultKey, printKeyName, printMetaLine } from "../../io/print";
import { queryMetaName, queryMetaValue } from "../../io/query";
import { completeList, completePath } from "../../services/completion";

export default {
  help: {
    head: "PATH [META_KEY [META_VALUE]] | -d PATH [META_KEY] | -l PATH...",
    description:
      "Modify/Delete/List meta data of an application or key. If an application with default key is " +
      "resolved, the application meta is still modified rather than the key meta.",
  },
  cmd: "meta",
  callback: meta,
  complete,
} as CommandExecutor;

function complete(currentParam: string, parameters: string[], idx: number) {
  if (currentParam.length > 0 && currentParam[0] === "-") {
    return "";
  }
  const flags = parseFlags(parameters);
  if (
    Reflect.has(flags.map, "l") ||
    flags.remainder.length === 0 ||
    (flags.remainder.length === 1 && idx < parameters.length)
  ) {
    return completePath(currentParam);
  }
  if (
    flags.remainder.length === 1 ||
    (flags.remainder.length === 2 && idx < parameters.length)
  ) {
    try {
      const stats = readStats(Path.fromString(flags.remainder[0]));
      if (!stats.isResolved) {
        return "";
      }
      const list = (stats.isKey
        ? (stats.key as Key)
        : (stats.app as Application)
      ).meta.map((m) => m.name);
      return completeList(currentParam, list);
      // eslint-disable-next-line no-empty
    } catch (ignored) {}
  }
  return "";
}

async function meta(parameters: string[]) {
  const flags = parseFlags(parameters);
  if (Reflect.has(flags.map, "l")) {
    for (const param of flags.remainder) {
      listMeta(param);
    }
  } else {
    const stats = readStats(Path.fromString(flags.remainder[0]));
    if (!stats.isResolved) {
      throw new CommandError("Path not found: " + flags.remainder[0]);
    }
    if (stats.isRoot) {
      throw new CommandError("No meta data possible: /");
    }
    let metaName = flags.remainder.length > 1 ? flags.remainder[1] : null;
    if (metaName === null) {
      metaName = await queryMetaName();
    }
    const meta = stats.isApp
      ? (stats.app as Application).meta
      : (stats.key as Key).meta;
    const idx = meta.findIndex((m) => m.name === metaName);
    if (Reflect.has(flags.map, "d")) {
      if (!~idx) {
        return errMetaNotFound(metaName);
      }
      meta.splice(idx, 1);
    } else {
      let metaValue = flags.remainder.length > 2 ? flags.remainder[2] : null;
      if (metaValue === null) {
        metaValue = await queryMetaValue(~idx ? meta[idx].value : undefined);
      }
      if (~idx) {
        meta[idx].value = metaValue;
      } else {
        meta.push({ value: metaValue, name: metaName });
      }
    }
    persist();
  }
}

function listMeta(path: string) {
  const stats = readStats(Path.fromString(path));
  let meta: Meta[];
  if (stats.isKey) {
    meta = (stats.key as Key).meta;
  } else if (stats.isApp) {
    meta = stats.isRoot ? [] : (stats.app as Application).meta;
  } else {
    return errPathNotFound(path);
  }
  if (meta.length === 0) {
    return errMetaNothing(path);
  }
  // todo move to io/
  STY_APP(pathFromApp(stats.app).toString(true));
  if (stats.isKey) {
    printKeyName(stats.key as Key);
  }
  STY_DEFAULT(EOL);
  if (stats.isApp && stats.key !== null) {
    printDefaultKey(stats.key);
  }
  printMetaLine(meta);
}
