import { CommandExecutor } from "../types/CommandExecutor";
import { getState, putState } from "../services/state";
import Path from "../services/path/Path";
import { appFromPath } from "../services/path/application";
import { parseFlags } from "../services/util";
import { completeApp, completePath } from "../services/completion";
import { printHashList, printNavigationList } from "../io/print";

const CMD_CD = {
  help: {
    head: "[APP]",
    description: "Change current application.",
    see: ["bookmark", "ls"],
  },
  cmd: "cd",
  callback: cd,
  complete: completeCd,
} as CommandExecutor;
const CMD_LS = {
  help: {
    head: "[-l] [-r | -d DEPTH] [PATH...]",
    description:
      "List children of PATH. Use '-d' to show more details (e.g. meta data). Use '-r' to list recursively.",
    see: ["cd"],
  },
  cmd: "ls",
  callback: ls,
  complete: completeLs,
} as CommandExecutor;
const CMD_HASH = {
  help: {
    head: "[-l | -r | -d DEPTH] [PATH...]",
    description:
      "List integrity hashes. Use '-l' to list all flat children or '-r' to list all recursive children. " +
      "Specify a custom depth with '-d'.",
  },
  cmd: "hash",
  callback: hash,
  complete: completeHash,
} as CommandExecutor;

export default [
  CMD_CD,
  CMD_LS,
  CMD_HASH,
  // CMD_FIND,
];

function completeCd(currentParam: string, parameters: string[], idx: number) {
  if (
    parameters.length === 0 ||
    (parameters.length === 1 && idx < parameters.length)
  ) {
    return completeApp(currentParam);
  }
  return "";
}

function completeLs(currentParam: string) {
  if (currentParam.length > 0 && currentParam[0] === "-") {
    return "";
  }
  return completePath(currentParam);
}

function completeHash(currentParam: string) {
  if (currentParam.length > 0 && currentParam[0] === "-") {
    return "";
  }
  return completePath(currentParam);
}

export function cd(parameters: string[]) {
  if (parameters.length === 0) {
    putState({ cwd: Path.fromString("~") });
  } else {
    const cwd = Path.join(getState().cwd, Path.fromString(parameters[0]));
    appFromPath(cwd); // validate existence of application
    putState({ cwd });
  }
}

function hash(parameters: string[]) {
  const flags = parseFlags(parameters, { d: 1 });
  const cwd = getState().cwd;
  if (flags.remainder.length === 0) {
    flags.remainder = ["."];
  }
  const paths = flags.remainder.map((str) =>
    Path.join(cwd, Path.fromString(str))
  );
  const depth = Reflect.has(flags.map, "r")
    ? Infinity
    : Reflect.has(flags.map, "d")
    ? +flags.map["d"].argv[0]
    : Reflect.has(flags.map, "l")
    ? 1
    : 0;
  printHashList(paths, depth, Reflect.has(flags.map, "l"));
}

function ls(parameters: string[]) {
  const flags = parseFlags(parameters, { d: 1 });
  const cwd = getState().cwd;
  if (flags.remainder.length === 0) {
    flags.remainder = ["."];
  }
  const paths = flags.remainder.map((str) =>
    Path.join(cwd, Path.fromString(str))
  );
  const depth = Reflect.has(flags.map, "d")
    ? +flags.map["d"].argv[0]
    : Reflect.has(flags.map, "r")
    ? Infinity
    : 0;
  printNavigationList(paths, depth, Reflect.has(flags.map, "l"));
}
