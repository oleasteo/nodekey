import { CommandExecutor } from "../types/CommandExecutor";
import { parseFlags } from "../services/util";
import { getData, persist } from "../services/profile";
import CommandError from "../types/CommandError";
import { fromSchema } from "../services/crypto/hashing";
import { getSettings } from "../services/settings";
import { hash } from "../services/crypto/index";
import { getState } from "../services/state";
import { errIntegrityFail, printIntegritySuccess } from "../io/feedback";
import { printIntegrityList } from "../io/print";
import { completeList } from "../services/completion";

const CMD_INTEGRITY = {
  help: {
    head: "[USER] | -n [USER] | -d [USER] | -l",
    description:
      "Validates the integrity of the current master key. If no parameter is passed, a simple master key " +
      "confirmation will be prompted and compared with the current master key. With '-n' a new integrity user can " +
      "be created for the current master key, to be used for verification without '-n'.",
  },
  cmd: "integrity",
  callback: integrity,
  complete,
} as CommandExecutor;

export default [CMD_INTEGRITY];

function complete(currentParam: string, parameters: string[], idx: number) {
  if (currentParam.length > 0 && currentParam[0] === "-") {
    return "";
  }
  const flags = parseFlags(parameters);
  if (!Reflect.has(flags.map, "l")) {
    return "";
  }
  if (
    flags.remainder.length === 0 ||
    (flags.remainder.length === 1 && idx < parameters.length)
  ) {
    return completeList(currentParam, Object.keys(getData().integrity));
  }
  return "";
}

async function integrity(parameters: string[]) {
  const flags = parseFlags(parameters);
  const name = flags.remainder.length > 0 ? flags.remainder.join(" ") : "";
  if (Reflect.has(flags.map, "l")) {
    list();
  } else if (Reflect.has(flags.map, "n")) {
    await register(name);
    await persist();
  } else if (Reflect.has(flags.map, "d")) {
    unregister(name);
    await persist();
  } else {
    await validate(name);
  }
}

async function register(name: string) {
  const integrityMap = getData().integrity;
  if (Reflect.has(integrityMap, name)) {
    throw new CommandError("Integrity data already exists.");
  }
  const methods = getSettings().hashing.map((method) => fromSchema(method));
  const value = await hash(methods, getPhrase(name), getState().masterKey, 32);
  integrityMap[name] = {
    methods,
    value,
  };
}

function unregister(name: string) {
  const integrityMap = getData().integrity;
  if (!Reflect.has(integrityMap, name)) {
    throw new CommandError("Integrity data not found.");
  }
  Reflect.deleteProperty(integrityMap, name);
}

function list() {
  printIntegrityList(getData().integrity);
}

async function validate(name: string) {
  const integrityMap = getData().integrity;
  if (!Reflect.has(integrityMap, name)) {
    throw new CommandError("Integrity data not found.");
  }
  const integrity = integrityMap[name];
  const value = await hash(
    integrity.methods,
    getPhrase(name),
    getState().masterKey,
    32
  );
  if (!value.equals(integrity.value)) {
    return errIntegrityFail();
  }
  printIntegritySuccess();
}

function getPhrase(name: string) {
  return Buffer.from(name || "default");
}
