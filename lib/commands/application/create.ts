import { parseFlags } from "../../services/util";
import { getState } from "../../services/state";
import { appFromPath, readStats } from "../../services/path/application";
import Path from "../../services/path/Path";
import {
  getApplications,
  getData,
  getRootApplications,
  getRootApplicationsList,
  persist,
} from "../../services/profile";
import { Application } from "../../types/Application";
import { cd } from "../navigation";
import { v4 as uuid } from "uuid";
import { _edit } from "./edit";
import { CommandExecutor } from "../../types/CommandExecutor";
import {
  errApplicationCreateNoParent,
  errPathAlreadyExists,
} from "../../io/feedback";
import { createKeyFor } from "../key/create";
import { completeApp } from "../../services/completion";
import { Key } from "../../types/Key";
import { catKeyContent, fetchKeyContent } from "../key/getter";

export default {
  help: {
    head: "[-p] [-S] [-w] [-k [-f] [-c]] APP...",
    description:
      "Creates a new application. '-p' enables creation of parent applications (without schema). '-S' " +
      "omits schema definition. '-c' changes the cwd to the newly created app (first passed APP). Use '-k' to " +
      "create a default key after creation; this is ignored in combination with '-S'. The '-f' and '-c' flags will " +
      "fetch/cat the (first) created key respectively.",
    see: ["edit", "mv", "rm"],
  },
  cmd: "mkapp",
  callback: createApp,
  complete,
} as CommandExecutor;

function complete(currentParam: string) {
  if (currentParam.length > 0 && currentParam[0] === "-") {
    return "";
  }
  return completeApp(currentParam);
}

async function createApp(parameters: string[]) {
  const flags = parseFlags(parameters);
  const state = getState();
  const paths = flags.remainder;
  const cwd = appFromPath(state.cwd);
  let pendingPersist = false;
  for (let i = 0; i < paths.length; i++) {
    const param = paths[i];
    const stats = readStats(Path.fromString(param), cwd);
    if (stats.isResolved) {
      errPathAlreadyExists(param);
      break;
    }
    const path = stats.relPath as Path;
    if (!Reflect.has(flags.map, "p") && path.reverseParts.length > 1) {
      errApplicationCreateNoParent(param);
      break;
    }
    // create application(s)
    let app = stats.app;
    for (let j = path.reverseParts.length - 1; j >= 0; j--) {
      app = _createApp(path.reverseParts[j], app);
      pendingPersist = true;
    }
    app = app as Application;
    // define schema
    if (!Reflect.has(flags.map, "S")) {
      await _edit(app);
      if (Reflect.has(flags.map, "k")) {
        const key = (await createKeyFor(app, true)) as Key;
        if (i === 0) {
          if (Reflect.has(flags.map, "c")) {
            await catKeyContent(key);
          }
          if (Reflect.has(flags.map, "f")) {
            await fetchKeyContent(key);
          }
        }
      }
      pendingPersist = true;
    }
  }
  if (Reflect.has(flags.map, "w")) {
    cd([paths[0]]);
  }
  pendingPersist && (await persist());
}

function registerApp(app: Application): Application {
  const applicationsList = getData().applications;
  const applications = getApplications();
  applicationsList.push(app);
  applications[app._id] = app;
  if (app.parent === null) {
    const rootApplicationsList = getRootApplicationsList();
    const rootApplications = getRootApplications();
    rootApplicationsList.push(app);
    rootApplications[app.name] = app;
  } else {
    const parent = applications[app.parent];
    parent.childrenList.push(app);
    parent.children[app.name] = app;
  }
  return app;
}

function _createApp(name: string, parent: Application | null): Application {
  return registerApp({
    _id: uuid(),
    cdate: Date.now(),
    name,
    parent: parent && parent._id,
    children: {},
    childrenList: [],
    meta: [],
    defaultKeyId: null,
    keys: {},
    keysList: [],
    schema: null,
  });
}
