import Path from "../../services/path/Path";
import { appFromPath } from "../../services/path/application";
import { getState } from "../../services/state";
import { persist } from "../../services/profile";
import { Application } from "../../types/Application";
import { CommandExecutor } from "../../types/CommandExecutor";
import { queryKeySchema } from "../../io/query";
import { printApplicationEditSchema } from "../../io/print";
import {
  errApplicationEditRoot,
  errApplicationNotSpecified,
} from "../../io/feedback";
import { completeApp } from "../../services/completion";

export default {
  help: {
    head: "APP",
    description: "Modify some application schema.",
    see: ["mkapp", "meta"],
  },
  cmd: "edit",
  callback: edit,
  complete,
} as CommandExecutor;

function complete(currentParam: string) {
  return completeApp(currentParam);
}

async function edit(parameters: string[]) {
  if (parameters.length === 0) {
    return errApplicationNotSpecified();
  }
  const path: Path = Path.fromString(parameters[0]);
  const app = appFromPath(Path.join(getState().cwd, path));
  if (app === null) {
    return errApplicationEditRoot();
  }
  await _edit(app);
  await persist();
}

export async function _edit(app: Application) {
  printApplicationEditSchema(app.name);
  app.schema = await queryKeySchema(app.schema || undefined);
}
