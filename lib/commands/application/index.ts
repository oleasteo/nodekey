import CMD_CREATE from "./create";
import CMD_EDIT from "./edit";

export default [CMD_CREATE, CMD_EDIT];
