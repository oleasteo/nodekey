import { omit } from "lodash";
import { Meta } from "./Meta";
import {
  CipherMethod,
  cipherMethodFromDTO,
  hashMethodFromDTO,
  HashMethod,
  cipherMethodToDTO,
  CipherMethodDTO,
  HashMethodDTO,
  hashMethodToDTO,
} from "./Method";

export type Key = HashKey | CipherKey;

interface KeyBase {
  _appId: string;
  _id: string;
  cdate: number;
  name: string;
  meta: Meta[];
}

interface HashKey extends KeyBase {
  hashed: true;
  methods: HashMethod[];
}

interface CipherKey extends KeyBase {
  hashed: false;
  methods: CipherMethod[];
  value: Buffer;
}

type KeyDTOBase = Omit<Key, "_appId" | "methods" | "value" | "hashed">;

type HashKeyDTO = KeyDTOBase & {
  hashed: true;
  methods: HashMethodDTO[];
};

type CipherKeyDTO = KeyDTOBase & {
  hashed: false;
  methods: CipherMethodDTO[];
  value: string;
};

export type KeyDTO = HashKeyDTO | CipherKeyDTO;

function isHashKeyDTO(dto: KeyDTO): dto is HashKeyDTO {
  return dto.hashed;
}

export function isHashKey(key: Key): key is HashKey {
  return key.hashed;
}

export function keyFromDTO(
  dto: KeyDTO
): Omit<HashKey, "_appId"> | Omit<CipherKey, "_appId"> {
  if (isHashKeyDTO(dto)) {
    return {
      ...dto,
      methods: dto.methods.map(hashMethodFromDTO),
    };
  } else {
    return {
      ...dto,
      methods: dto.methods.map(cipherMethodFromDTO),
      value: Buffer.from(dto.value, "base64"),
    };
  }
}

export function keyToDTO(key: Key): KeyDTO {
  if (isHashKey(key)) {
    return {
      ...omit(key, ["_appId"]),
      methods: key.methods.map(hashMethodToDTO),
    };
  } else {
    return {
      ...omit(key, ["_appId"]),
      methods: key.methods.map(cipherMethodToDTO),
      value: key.value.toString("base64"),
    };
  }
}
