import { CipherMethodSchema, HashMethodSchema } from "./Method";

export interface BaseSettings {
  file_encoding: string;

  encryption: CipherMethodSchema[];
  hashing: HashMethodSchema[];

  char_sets: CharSet[];
  char_set_custom: CustomCharSet;
  char_set_collections: CharSetCollection[];
  default_length: number;

  output: {
    prompt: {
      meta: boolean; // whether to show the application meta within prompt
    };
    fetch: {
      meta: boolean; // whether to show the application key meta; otherwise bg-color is used as fg-color
      print: boolean;
      hidden: boolean;
      clipboards: Clipboard[];
    };
  };
}

export interface Settings extends BaseSettings {
  interactive?: BaseSettingsOptional;
}

export interface CharSet {
  id: number;
  name: string;
  characters: string;
  checked: boolean;
}

export interface CustomCharSet {
  name: string;
  checked: boolean;
}

export function isCustomCharSet(
  charSet: CharSet | CustomCharSet
): charSet is CustomCharSet {
  return !Reflect.has(charSet, "characters");
}

interface CharSetCollection {
  name: string;
  char_set_ids: number[];
}

interface Clipboard {
  // boards are ignored for now; underlying library does not support those
  boards: string[]; // the system IDs of clipboards to use (boards that don't exist are ignored)
  timeout: number | null; // if >0 clear clipboard after time in ms
}

interface BaseSettingsOptional {
  char_sets?: CharSet[];
  char_set_collections?: CharSetCollection[];
  default_length?: number;

  output?: {
    prompt?: { meta?: boolean };
    fetch?: {
      meta?: boolean;
      print?: boolean;
      hidden?: boolean;
      clipboards?: Clipboard[];
    };
  };
}
