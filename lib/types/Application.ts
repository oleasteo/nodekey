import { Meta } from "./Meta";
import { keyFromDTO, Key, keyToDTO, KeyDTO } from "./Key";
import { mapKeys, mapValues, omit } from "lodash";

export interface Application {
  _id: string;
  parent: string | null;
  children: { [name: string]: Application };
  childrenList: Application[];
  cdate: number;
  defaultKeyId: string | null;
  name: string;
  meta: Meta[];
  schema: KeySchema | null;
  keys: { [name: string]: Key };
  keysList: Key[];
}

export type StandaloneApplication = Omit<
  Application,
  "children" | "childrenList"
>;

export interface ApplicationDTO
  extends Omit<Application, "keys" | "keysList" | "children" | "childrenList"> {
  keysList: KeyDTO[];
}

export interface KeySchema {
  length: number;
  characters: string;
}

export function applicationFromDTO(dto: ApplicationDTO): StandaloneApplication {
  return {
    ...dto,
    keysList: dto.keysList.map(keyFromDTO).map((it) => ({
      _appId: dto._id,
      ...it,
    })),
    keys: mapValues(mapKeys(dto.keysList, "name"), (it) => ({
      _appId: dto._id,
      ...keyFromDTO(it),
    })),
  };
}

export function applicationToDTO(application: Application): ApplicationDTO {
  return {
    ...omit(application, ["children", "childrenList", "keys"]),
    keysList: application.keysList.map(keyToDTO),
  };
}
