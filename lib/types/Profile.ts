import {
  Application,
  applicationFromDTO,
  applicationToDTO,
  ApplicationDTO,
} from "./Application";
import {
  hashMethodFromDTO,
  HashMethod,
  HashMethodDTO,
  hashMethodToDTO,
} from "./Method";
import { mapKeys, mapValues } from "lodash";
import CommandError from "./CommandError";

export interface Profile {
  integrity: { [ID: string]: Integrity };
  bookmarks: { [ID: string]: string | null };
  applications: Application[];
  aliases: { [alias: string]: string };
}

interface ProfileDTO extends Omit<Profile, "integrity" | "applications"> {
  integrity: { [ID: string]: IntegrityDTO };
  applications: ApplicationDTO[];
}

export interface Integrity {
  methods: HashMethod[];
  value: Buffer;
}

interface IntegrityDTO {
  methods: HashMethodDTO[];
  value: string;
}

export function profileFromDTO(dto: ProfileDTO): Profile {
  const applications: Application[] = dto.applications
    .map(applicationFromDTO)
    .map((it) => ({
      ...it,
      children: {},
      childrenList: [],
    }));
  const applicationMap = mapKeys(applications, "_id");
  // saturate app hierarchy, modifying applications in-place
  for (const app of applications) {
    if (app.parent !== null) {
      if (!Reflect.has(applicationMap, app.parent)) {
        throw new CommandError(
          `Profile corrupted. Parent of application '${app.name}' not found.`
        );
      }
      const parent = applicationMap[app.parent];
      parent.childrenList.push(app);
      parent.children[app.name] = app;
    }
  }
  // return profile
  return {
    ...dto,
    integrity: mapValues(dto.integrity, integrityFromDTO),
    applications,
  };
}

export function profileToDTO(profile: Profile): ProfileDTO {
  return {
    ...profile,
    integrity: mapValues(profile.integrity, integrityToDTO),
    applications: profile.applications.map(applicationToDTO),
  };
}

export function integrityFromDTO(dto: IntegrityDTO): Integrity {
  return {
    methods: dto.methods.map(hashMethodFromDTO),
    value: Buffer.from(dto.value, "base64"),
  };
}

export function integrityToDTO(integrity: Integrity): IntegrityDTO {
  return {
    methods: integrity.methods.map(hashMethodToDTO),
    value: integrity.value.toString("base64"),
  };
}
