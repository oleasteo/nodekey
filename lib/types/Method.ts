import { CipherCCMOptions, CipherGCMOptions } from "crypto";

interface CipherAlgorithm {
  name: string;
  keyBytes: number;
}

export interface SCryptOptions {
  cost?: number; // CPU/memory cost parameter. Must be a power of two greater than one. Default: 16384.
  blockSize?: number; // Block size parameter. Default: 8.
  parallelization?: number; // Parallelization parameter. Default: 1.
  maxmem?: number; // Memory upper bound. Default: 32 * 1024 * 1024.
  // Requires: 128 * cost * blockSize <= maxmem
}

export interface CipherMethodSchema {
  algorithm: CipherAlgorithm;
  saltBytes: number;
  ivBytes: number;
  options?: CipherCCMOptions & CipherGCMOptions;
  keyOptions?: SCryptOptions;
}

export interface CipherMethod {
  algorithm: CipherAlgorithm;
  salt: Buffer;
  iv: Buffer;
  options?: CipherCCMOptions & CipherGCMOptions;
  keyOptions?: SCryptOptions;
}

export interface CipherMethodDTO extends Omit<CipherMethod, "salt" | "iv"> {
  salt: string;
  iv: string;
}

interface HashAlgorithm {
  name: string;
}

export interface HashMethodSchema {
  algorithm: HashAlgorithm;
  saltBytes: number;
  options?: SCryptOptions;
}

export interface HashMethod {
  algorithm: HashAlgorithm;
  salt: Buffer;
  options?: SCryptOptions;
}

export interface HashMethodDTO extends Omit<HashMethod, "salt"> {
  salt: string;
}

export function cipherMethodFromDTO(dto: CipherMethodDTO): CipherMethod {
  return {
    ...dto,
    salt: Buffer.from(dto.salt, "base64"),
    iv: Buffer.from(dto.iv, "base64"),
  };
}

export function hashMethodFromDTO(dto: HashMethodDTO): HashMethod {
  return {
    ...dto,
    salt: Buffer.from(dto.salt, "base64"),
  };
}

export function cipherMethodToDTO(cipherMethod: CipherMethod): CipherMethodDTO {
  return Object.assign({}, cipherMethod, {
    salt: cipherMethod.salt.toString("base64"),
    iv: cipherMethod.iv.toString("base64"),
  });
}

export function hashMethodToDTO(hashMethod: HashMethod): HashMethodDTO {
  return Object.assign({}, hashMethod, {
    salt: hashMethod.salt.toString("base64"),
  });
}
