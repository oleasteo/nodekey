import { CompletionResult } from "terminal-kit";

export interface CommandCallback {
  (parameters: string[], argv: string[], executors: CommandExecutor[]):
    | Promise<boolean | void>
    | boolean
    | void;
}

export interface CommandExecutor {
  help: {
    head?: string;
    description: string;
    see?: string[];
  };
  cmd: string;
  callback: CommandCallback;
  complete?: (
    currentParameter: string,
    parameters: string[],
    currentIdx: number
  ) => CompletionResult;
}
