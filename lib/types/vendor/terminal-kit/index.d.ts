// The ts-essentials types are lacking some details, fill in ourselves...

declare module "terminal-kit" {
  import EventEmitter = NodeJS.EventEmitter;
  export type CompletionResult = string | CompletionArray;
  export const terminal: Terminal;

  interface CompletionArray extends Array<string> {
    _truncate?: number; // internal, not from terminal-kit; just for convenience
    prefix?: string;
    postfix?: string;
  }

  enum SingleLineMenuKeyAction {
    SUBMIT = "submit",
    PREVIOUS = "previous",
    NEXT = "next",
    CYCLE_PREVIOUS = "cyclePrevious",
    CYCLE_NEXT = "cycleNext",
    PREVIOUS_PAGE = "previousPage",
    NEXT_PAGE = "nextPage",
    FIRST = "first",
    LAST = "last",
    ESCAPE = "escape",
  }

  enum SingleColumnMenuKeyAction {
    SUBMIT = "submit",
    PREVIOUS = "previous",
    NEXT = "next",
    CYCLE_PREVIOUS = "cyclePrevious",
    CYCLE_NEXT = "cycleNext",
    FIRST = "first",
    LAST = "last",
    ESCAPE = "escape",
  }

  enum InputFieldKeyAction {
    SUBMIT = "submit",
    CANCEL = "cancel",
    BACK_DELETE = "backDelete",
    DELETE = "delete",
    DELETE_ALL_BEFORE = "deleteAllBefore",
    DELETE_ALL_AFTER = "deleteAllAfter",
    BACKWARD = "backward",
    FORWARD = "forward",
    PREVIOUS_WORD = "previousWord",
    NEXT_WORD = "nextWord",
    HISTORY_PREVIOUS = "historyPrevious",
    HISTORY_NEXT = "historyNext",
    START_OF_INPUT = "startOfInput",
    END_OF_INPUT = "endOfInput",
    AUTOCOMPLETE = "autoComplete",
    AUTOCOMPLETE_USING_HISTORY = "autoCompleteUsingHistory",
    META = "meta",
  }

  enum HorizontalAlignment {
    LEFT = "left",
    RIGHT = "right",
    CENTER = "center",
  }

  interface SingleLineMenuOptions {
    y?: number;
    separator?: string;
    nextPageHint?: string;
    previousPageHint?: string;
    style?: Terminal;
    selectedStyle?: Terminal;
    selectedIndex?: number;
    align?: HorizontalAlignment;
    fillIn?: boolean;
    keyBindings?: Record<string, SingleLineMenuKeyAction>;
  }

  interface SingleColumnMenuOptions {
    y?: number;
    style?: Terminal;
    selectedStyle?: Terminal;
    submittedStyle?: Terminal;
    leftPadding?: string;
    selectedLeftPadding?: string;
    submittedLeftPadding?: string;
    extraLines?: number;
    oneLineItem?: boolean;
    itemMaxWidth?: number;
    continueOnSubmit?: boolean;
    selectedIndex?: number;
    keyBindings?: Record<string, SingleColumnMenuKeyAction>;
    cancelable?: boolean;
    exitOnUnexpectedKey?: boolean;
  }

  interface InputFieldOptions {
    x?: number;
    echo?: boolean;
    echoChar?: string | true;
    default?: string;
    cursorPosition?: number;
    cancelable?: boolean;
    style?: Terminal;
    hintStyle?: Terminal;
    maxLength?: number;
    minLength?: number;
    history?: string[];
    autoComplete?:
      | string[]
      | ((input: string) => CompletionResult | Promise<CompletionResult>);
    autoCompleteMenu?: boolean | SingleLineMenuOptions;
    autoCompleteHint?: boolean;
    keyBindings?: Record<string, InputFieldKeyAction>;
    tokenHook?: unknown;
    tokenResetHook?: unknown;
    tokenRegExp?: RegExp;
  }

  interface Terminal extends EventEmitter {
    (...values: Array<string | number | boolean | null | undefined>): Terminal;

    //Properties
    width: number;
    height: number;

    //Foreground colors
    defaultColor: Terminal;
    black: Terminal;
    red: Terminal;
    green: Terminal;
    yellow: Terminal;
    blue: Terminal;
    magenta: Terminal;
    cyan: Terminal;
    white: Terminal;
    brightBlack: Terminal;
    gray: Terminal;
    brightRed: Terminal;
    brightGreen: Terminal;
    brightYellow: Terminal;
    brightBlue: Terminal;
    brightMagenta: Terminal;
    brightCyan: Terminal;
    brightWhite: Terminal;
    color: Terminal;
    darkColor: Terminal;
    brightColor: Terminal;
    color256: Terminal;
    colorRgb: Terminal;
    colorRgbHex: Terminal;
    colorGrayscale: Terminal;

    //Background colors
    bgDefaultColor: Terminal;
    bgBlack: Terminal;
    bgRed: Terminal;
    bgGreen: Terminal;
    bgYellow: Terminal;
    bgBlue: Terminal;
    bgMagenta: Terminal;
    bgCyan: Terminal;
    bgWhite: Terminal;
    bgDarkColor: Terminal;
    bgBrightBlack: Terminal;
    bgGray: Terminal;
    bgBrightRed: Terminal;
    bgBrightGreen: Terminal;
    bgBrightYellow: Terminal;
    bgBrightBlue: Terminal;
    bgBrightMagenta: Terminal;
    bgBrightCyan: Terminal;
    bgColor: Terminal;
    bgBrightWhite: Terminal;
    bgBrightColor: Terminal;
    bgColor256: Terminal;
    bgColorRgb: Terminal;
    bgColorRgbHex: Terminal;
    bgColorGrayscale: Terminal;

    //Styles
    styleReset: Terminal;
    bold: Terminal;
    dim: Terminal;
    italic: Terminal;
    underline: Terminal;
    blink: Terminal;
    inverse: Terminal;
    hidden: Terminal;
    strike: Terminal;

    //Moving the Cursor
    saveCursor: Terminal;
    restoreCursor: Terminal;
    up: Terminal;
    down: Terminal;
    right: Terminal;
    left: Terminal;
    nextLine: Terminal;
    previousLine: Terminal;
    column: Terminal;
    scrollUp: Terminal;
    scrollDown: Terminal;
    scrollingRegion: Terminal;
    resetScrollingRegion: Terminal;
    moveTo: Terminal;
    move: Terminal;
    hideCursor: Terminal;
    tabSet: Terminal;
    tabClear: Terminal;
    tabClearAll: Terminal;
    forwardTab: Terminal;
    backwardTab: Terminal;

    //Editing the Screen
    clear: Terminal;
    eraseDisplayBelow: Terminal;
    eraseDisplayAbove: Terminal;
    eraseDisplay: Terminal;
    eraseScrollback: Terminal;
    eraseLineAfter: Terminal;
    eraseLineBefore: Terminal;
    eraseLine: Terminal;
    eraseArea: Terminal;
    insertLine: Terminal;
    deleteLine: Terminal;
    insert: Terminal;
    delete: Terminal;
    backDelete: Terminal;
    // scrollUp: Terminal;
    // scrollDown: Terminal;
    alternateScreenBuffer: Terminal;

    //Input/Output
    requestCursorLocation: Terminal;
    requestScreenSize: Terminal;
    requestColor: Terminal;
    applicationKeypad: Terminal;

    //Internal input/output (do not use directly, use grabInput() instead)
    mouseButton: Terminal;
    mouseDrag: Terminal;
    mouseMotion: Terminal;
    mouseSGR: Terminal;
    focusEvent: Terminal;

    //Operating System
    cwd: Terminal;
    windowTitle: Terminal;
    iconName: Terminal;
    notify: Terminal;

    //Modifiers
    error: Terminal;
    str: Terminal;
    noFormat: Terminal;
    markupOnly: Terminal;
    bindArgs: Terminal;

    //Misc
    reset: Terminal;
    bell: Terminal;
    setCursorColor: Terminal;
    setCursorColorRgb: Terminal;
    resetCursorColorRgb: Terminal;
    setDefaultColorRgb: Terminal;
    resetDefaultColorRgb: Terminal;
    setDefaultBgColorRgb: Terminal;
    resetDefaultBgColorRgb: Terminal;
    setHighlightBgColorRgb: Terminal;
    resetHighlightBgColorRgb: Terminal;
    processExit: (code?: number) => void;
    asyncCleanup: () => Promise<void>;
    grabInput: (options?: false | unknown, safe?: boolean) => void;

    // Input
    inputField: (
      options?: InputFieldOptions,
      cb?: (error: unknown, input: string) => void
    ) => InputReturn;
    singleColumnMenu: (
      menuItems: string[],
      options?: SingleColumnMenuOptions,
      cb?: (error: unknown, response: MenuResponse) => void
    ) => MenuReturn;
  }

  interface MenuResponse {
    selectedIndex: number;
    selectedText: string;
    submitted: boolean;
    x: number;
    y: number;
    canceled: boolean;
    unexpectedKey?: string;
  }

  interface InputReturn extends EventEmitter {
    abort: () => void;
    stop: () => void;
    getInput: () => string;
    getPosition: () => { x: number; y: number };
    getCursorPosition: () => void;
    setCursorPosition: (offset: number) => void;
    redraw: () => void;
    hide: () => void;
    show: () => void;
    rebase: (x?: number, y?: number) => void;

    promise: Promise<string>;
  }

  interface MenuReturn extends EventEmitter {
    abort: () => void;
    stop: (eraseMenu: boolean) => void;
    select: (index: number) => void;
    submit: () => void;
    cancel: () => void;
    erase: () => void;
    pause: () => void;
    resume: () => void;
    focus: (value: boolean) => void;
    getPosition: () => { x: number; y: number };
    redraw: () => void;
    redrawCursor: () => void;
    hide: () => void;
    show: () => void;
    rebase: () => void;
    getState: () => MenuResponse;

    promise: Promise<MenuResponse>;
  }
}
