import { terminal } from "terminal-kit";

export const STY_DEFAULT = terminal;
export const STY_ERROR = terminal.error.red;
export const STY_CTRL = terminal.yellow;

export const STY_HELP_NAME = terminal.bold;
export const STY_HELP_DESCRIPTION = terminal.italic;
export const STY_HELP_KEYWORD = terminal.bold;
export const STY_HELP_HEAD = terminal;
export const STY_HELP_SEE = terminal.underline;

export const STY_INIT_PATHS = terminal.brightBlack;

export const STY_APP = terminal.brightYellow;
export const STY_KEY_HASHED = terminal.green;
export const STY_KEY_STORED = terminal.brightGreen;

export const STY_PROMPT_DEFAULT = terminal;
export const STY_PROMPT_CWD = STY_APP;
export const STY_PROMPT_NUM_SHELLS = terminal.brightBlue.bold;
export const STY_PROMPT_NUM_APPS = STY_APP;
export const STY_PROMPT_NUM_KEYS = STY_KEY_HASHED;
export const STY_PROMPT_META_DEFAULT = terminal.brightBlack;
export const STY_PROMPT_META_KEY = terminal.brightBlack.bold;
export const STY_PROMPT_META_VALUE = terminal.white;
export const STY_PROMPT_CARET = STY_PROMPT_DEFAULT;

export const STY_INFO_SALT = STY_DEFAULT.bgBlue;
export const STY_INFO_CONTENT = terminal.underline;
export const STY_INFO_CONTENT_HIDDEN = terminal.bgWhite.white;

export const STY_ALIAS_NAME = terminal.white.bold;
export const STY_ALIAS_CMD = terminal.white;

export const STY_INTEGRITY_DEFAULT = terminal.white.bold;
export const STY_INTEGRITY_NAME = terminal.white;
export const STY_INTEGRITY_SUCCESS = terminal.green;

export const STY_BOOKMARK_NAME = STY_ALIAS_NAME;
export const STY_BOOKMARK_PATH = STY_ALIAS_CMD;
